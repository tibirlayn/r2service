<?php

$link = mysqli_connect('localhost', 'tilann_r2service', '756SAd179wte', 'tilann_r2service') or die("Ошибка " . mysqli_error($link));

if (!$link){
    echo "Ошибка: Невозможно установить соединение с MySQL. ".PHP_EOL.'<br>';
    echo "Код ошибки errno: ".mysqli_connect_errno().PHP_EOL.'<br>';
    echo "Тест ошибки error ".mysqli_connect_error().PHP_EOL.'<br>';
    exit;
}

if ($_SERVER['REQUEST_URI'] == "/") include 'all/main.php';
else {
    $page = substr($_SERVER['REQUEST_URI'], 1);
    if (!preg_match('/^[A-z0-9]{2,15}$/', $page)) exit ('URL ERROR');

    session_start();

    if (file_exists('all/'.$page.'.php')) include 'all/'.$page.'.php';
    else if (file_exists('admin/'.$page.'.php')) include 'admin/'.$page.'.php';
    else if (file_exists('error/'.$page.'.php')) include 'error/'.$page.'.php';
    else if ($_SESSION['uother'] == 1 and file_exists('other/'.$page.'.php')) include 'other/'.$page.'.php';
    else exit ('404 ERROR');
}

function message($text){
    exit('{ "message" : "'.$text.'" }');
}

function go( $url ) {
    exit('{ "go" : "'.$url.'" }');
}

function random_str( $num = 30){
    return substr(str_shuffle('0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'), 0, $num);
}

function email_valid(){
    if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)){
        message('E-mail указан неверно!');
    }
}

function password_valid(){
    if (!preg_match('/^[A-z0-9]{6,20}$/', $_POST['password'])){
        message('Пароль указан неверно!');
    }
}

class register{
    function registration($login,$email,$password){
        var_dump($login,$email,$password);
    }

    function login(){

    }

    function recovery(){

    }

    function messages(){

    }
}



function top($title){
    echo '
            <!DOCTYPE html>
            <html>
            <head>
                <!--Import Google Icon Font-->
                <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
                <!--Import materialize.css-->
                <link type="text/css" rel="stylesheet" href="/css/materialize.min.css"  media="screen,projection"/>
            
                <link rel="shortcut icon" href="/images/main/main.jpg" type="image/jpeg">
                <link rel="stylesheet" href="/css/style.css">
                <!--Let browser know website is optimized for mobile-->
                <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
                
                 <title>'.$title.'</title>
                 
                 <script type="text/javascript" src="/js/jquery-3.2.1.js"></script>
                 <script type="text/javascript" src="/js/materialize.min.js"></script>
            </head>
            <body>            
        ';
}

function bot(){
    echo '
                <footer class="page-footer white">
                <div class="footer-copyright white">
                    <div class="container" id="footer">
                            <p>Copyright © 2018 <a href="https://vk.com/tibirlayn">Zaharov Artur</a>
                            <a class="right" href="#">Tibirlayn</a>
                            </p>
                    </div>
                </div>
                </footer>
           </body>
           </html>
       ';
}

function top_admin($title) {
    echo '
        <!DOCTYPE html>
        <html>
        <head>
            <!--Import Google Icon Font-->
            <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
            <!--Import materialize.css-->
            <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
        
            <title>'.$title.'</title>
        
            <link rel="shortcut icon" href="images/main/main.jpg" type="image/jpeg">
            <link rel="stylesheet" href="css/style.css">
            <!--r2service.php-->
            <link rel="stylesheet" href="/css/nouislider.css">
            <link rel="stylesheet" href="/css/ion.rangeSlider.skinFlat.css">
            <link rel="stylesheet" href="/css/ion.rangeSlider.css">
            <!--Let browser know website is optimized for mobile-->
            <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
            
            <script type="text/javascript" src="/js/jquery-3.2.1.js"></script>
            <script type="text/javascript" src="/js/materialize.min.js"></script>
            <script type="text/javascript" src="/js/side-nav.js"></script>
        </head>
        <body>
        <!--sidenav-->
        <div class="navbar-upper ">
            <nav class="z-depth-0">
                <div class="nav-wrapper light-blue lighten-2">
                    <div class="row">
                        <div class="col s12">
                            <ul id="slide-out" class="sidenav">
                                <li><div class="user-view">
                                        <div class="background">
                                            <img src="images/main/office.jpg">
                                        </div>
                                        <a href=""><img class="circle" src="images/admin/admin.ico"></a>
                                        <a href=""><span class="white-text name">John Doe</span></a>
                                        <a href=""><span class="white-text email">jdandturk@gmail.com</span></a>
                                    </div></li>
                                <li><a href="r2admin">ГЛАВНАЯ</a></li>
                                <li><a href="r2equipment">ЭКИПИРОВКА</a></li>
                                <li><a href="r2material">МАТЕРИАЛЫ</a></li>
                                <li><a href="r2sphere">СФЕРЫ</a></li>
                                <li><a href="r2runes">РУНЫ</a></li>
                                <li><a href="r2other">ДРУГОЕ</a></li>
                                <li><a href="r2monster">МОНСТРЫ</a></li>
                                <li><a href="r2services">УСЛУГИ</a></li>
                                <li><a href="r2register">ADMIN</a></li>
                            </ul>
        
                            <a  href="#" data-target="slide-out" class="sidenav-trigger" style="display:block;float:left;"><i class="material-icons">menu</i></a>
        
                            <ul id="nav-mobile" class="right hide-on-med-and-down">
        
                                <li>
        
                                    <form style="height:64px;">
                                        <div class="input-field ">
                                            <input id="search" type="search" required placeholder="Search">
                                            <label class="label-icon" for="search"><i class="this material-icons">search</i></label>
                                        </div>
                                    </form>
        
                                </li>
                                <li><a class=\'dropdown-trigger btn\' href=\'#\' data-target=\'dropdown1\'>Услуги</a>
        
                                    <ul id=\'dropdown1\' class=\'dropdown-content\'>
                                        <li><a href="">Персонажи</a></li>
                                        <li><a href="">Гарант</a></li>
                                        <li><a href="">Серебро</a></li>
                                        <li><a href="">Кидалы</a></li>
                                        <li><a href="">Инфо</a></li>
                                    </ul>
                                </li>
        
                                <li><a href="r2services_add"><i class="material-icons">add</i></a></li>
        
                            </ul>
        
                        </div>
                    </div>
                </div>
            </nav>
        </div>
    ';
}


function top_photosession($title) {
    echo '
        <!DOCTYPE html>
        <html>
        <head>
            <!--Import Google Icon Font-->
            <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
            <!--Import materialize.css-->
            <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
        
            <title>'.$title.'</title>
        
            <link rel="shortcut icon" href="images/main/main.jpg" type="image/jpeg">
            <link rel="stylesheet" href="css/style.css">
            <!--r2service.php-->
            <link rel="stylesheet" href="/css/nouislider.css">
            <link rel="stylesheet" href="/css/ion.rangeSlider.skinFlat.css">
            <link rel="stylesheet" href="/css/ion.rangeSlider.css">
            <!--Let browser know website is optimized for mobile-->
            <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
            
            <script type="text/javascript" src="/js/jquery-3.2.1.js"></script>
            <script type="text/javascript" src="/js/materialize.min.js"></script>
            <script type="text/javascript" src="/js/side-nav.js"></script>
        </head>
        <body>
        <!--sidenav-->
        <div class="navbar-upper ">
            <nav class="z-depth-0">
                <div class="nav-wrapper purple darken-1">
                    <div class="row">
                        <div class="col s12">
                            <ul id="slide-out" class="sidenav">
                                <li><div class="user-view">
                                        <div class="background">
                                            <img src="images/photosession/office.jpg">
                                        </div>
                                        <a href=""><img class="circle" src="images/photosession/admin.jpg"></a>
                                        <a href="https://vk.com/id95960493"><span class="white-text name">Павел Абеляшев</span></a>
                                        <br>
                                    </div></li>
                                <li><a href="photosession">ГЛАВНАЯ</a></li>
                                <li><a href="photo">ФОТО</a></li>
                            </ul>
        
                            <a  href="#" data-target="slide-out" class="sidenav-trigger" style="display:block;float:left;"><i class="material-icons">menu</i></a>
        
                            <ul id="nav-mobile" class="right hide-on-med-and-down">              

                            </ul>
        
                        </div>
                    </div>
                </div>
            </nav>
        </div>
    ';
}

function bot_photosession() {
    echo '
        <footer class="page-footer purple darken-1">
          <div class="container">
            <div class="row">
              <div class="col l6 s12">
                <h5 class="white-text">Footer Content</h5>
                <p class="grey-text text-lighten-4">You can use rows and columns here to organize your footer content.</p>
              </div>
              <div class="col l4 offset-l2 s12">
                <h5 class="white-text">Links</h5>
                <ul>
                  <li><a class="grey-text text-lighten-3" href="#!">Link 1</a></li>
                </ul>
              </div>
            </div>
          </div>
          <div class="footer-copyright">
            <div class="container">
            © 2018 Abelyashev Pavel
            <a class="grey-text text-lighten-4 right" href="https://vk.com/id95960493">Павел Абеляшев</a>
            </div>
          </div>
        </footer>
    ';
}
?>