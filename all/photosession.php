<?php top_photosession('ФотоСессия'); ?>
    <div class="container">
    <div class="row">
<?php
    $query = "SELECT * FROM `photosession`";
    $query_data = mysqli_query($link, $query);

    if (mysqli_num_rows($query_data) > 0){
        $row_photosession = mysqli_fetch_array($query_data);
        do {
            if ($row_photosession["image"] != "" && file_exists("images/photosession/".$row_photosession["image"].".jpg")){
                $img_path = 'images/photosession/'.$row_photosession["image"].".jpg";
            } else {
                $img_path = "../images/photosession/no-icon.jpg";
            }
            echo '
                       <div class="col s12 m6 l6 xl6">
                           <div class="card">
                            <div class="card-image">
                              <img class="materialboxed" src="'.$img_path.'">
                            </div>
                          </div>                                
                       </div>
            ';
        } while ($row_photosession = mysqli_fetch_array($query_data));
    }
?>
    </div>
    </div>

<script>
    document.addEventListener('DOMContentLoaded', function() {
        var elems = document.querySelectorAll('.materialboxed');
        var instances = M.Materialbox.init(elems, options);
    });

    $(document).ready(function(){
        $('.materialboxed').materialbox();
    });
</script>
<?php bot_photosession(); ?>