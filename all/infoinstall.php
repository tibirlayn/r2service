<?php
    top('info');
?>
    <nav>
        <div class="nav-wrapper light-blue lighten-2">
            <a href="" class="brand-logo" style="margin-left: 10px">INFO</a>
            <ul class="right hide-on-med-and-down">
                <li><a href="infoinstall">Установка программного обеспечения</a></li>
                <li><a href="infojob">Регламентные работы</a></li>
                <li><a href="info">Помощь</a></li>
            </ul>
        </div>
    </nav>
<div class="container">
    <h5 class="left-align">Установка программного обеспечения</h5>

    <h5 class="left-align">Google Chrome</h5>
    <b> Установка под Windows</b> <br>

    Первое, что нужно для установки Google Chrome – скачать установочный файл. Можно это сделать на официальном сайте
    https://www.google.ru/chrome/browser/desktop/. В открывшейся вкладке нажмите на большую синюю кнопку «Скачать Chrome» .
    <br>

    <img class="materialboxed" width="650" src="../images/error/google1.png"> <br>

    Откроется окно с параметрами установки Google Chrome. Желательно убрать галочку напротив «Разрешить автоматическую
    отправку статистики использования и отчетов о сбоях» . Если вы не планируете использовать Google Chrome как
    основной браузер, тогда также уберите галочку напротив «Установить Google
    Chrome в качестве браузера по умолчанию» . Нажмите кнопку «Принять условия и установить» .<br>

    <img class="materialboxed" width="650" src="../images/error/google2.png"> <br>
    Подождите пока скачается установщик Google Chrome. Если у вас появится окно «Сохранить как..» , выберите «Рабочий стол» и нажмите «Сохранить» .<br>

    Установка Google Chrome <br>
    После закачки Google Chrome установка может начаться автоматически. Если не начнется, тогда вам нужно самостоятельно
    запустить только что скачанный файл ChromeSetup.exe с рабочего стола или с меню «Загрузки» в текущем браузере.<br>
    <img class="materialboxed" width="650" src="../images/error/google3.png"> <br>
    Подождите пока установщик Chrome загрузит необходимые файлы из интернета и установит браузер на ваш компьютер.
    Процесс полностью автоматический, ваше участие дальше не требуется. Веб-браузер Google
    Chrome запустится сразу же после завершения процесса установки. Теперь у вас на компьютере есть быстрый и современный браузер.<br>

</div>

<script>
    document.addEventListener('DOMContentLoaded', function() {
        var elems = document.querySelectorAll('.collapsible');
        var instances = M.Collapsible.init(elems, options);
    });

    // Or with jQuery

    $(document).ready(function(){
        $('.collapsible').collapsible();
    });

    $(document).ready(function(){
        $('.materialboxed').materialbox();
    });
</script>
<?php
    bot();
?>