<?php

    ini_set('display_errors', 1);
    error_reporting(E_ALL);

    top('Crowfall');
    echo '
        <ul id="dropdown1" class="dropdown-content">
            <li><a href="">Производственные</a></li>
            <li><a href="">Свитки усиления</a></li>
            <li><a href="">Жезлы</a></li>
            <li><a href="">Книги</a></li>
            <li><a href="">Другое</a></li>
        </ul>
        <ul id="dropdown2" class="dropdown-content">
            <li><a href="">Оружие ближнего боя</a></li>
            <li><a href="">Оружие дальнего боя</a></li>
            <li><a href="">Доспехи</a></li>
            <li><a href="">Доп. защита</a></li>
            <li><a href="">Перчатки</a></li>
            <li><a href="">Сапоги</a></li>
            <li><a href="">Шлемы</a></li>
            <li><a href="">Плащи</a></li>
            <li><a href="">Аксессуары</a></li>
        </ul>
        <ul id="dropdown3" class="dropdown-content">
            <li><a href="">Сферы души</a></li>
            <li><a href="">Сферы жизни</a></li>
            <li><a href="">Сферы мастерства</a></li>
            <li><a href="">Сферы защиты</a></li>
            <li><a href="">Сферы разрушения</a></li>
            <li><a href="">Сферы характеристик</a></li>
        </ul>
        <ul id="dropdown4" class="dropdown-content">
            <li><a href="">Руны оружия</a></li>
            <li><a href="">Руны доспехов</a></li>
            <li><a href="">Руны шлемов</a></li>
            <li><a href="">Руны перчаток</a></li>
            <li><a href="">Руны сапог</a></li>
            <li><a href="">Руны плащей</a></li>
            <li><a href="">Руны браслетов/щитов</a></li>
            <li><a href="">Временные руны</a></li>
            <li><a href="">Сундуки с рунами</a></li>
        </ul>
        <ul id="dropdown5" class="dropdown-content">
            <li><a href="">Перевоплощения</a></li>
            <li><a href="">Питомец</a></li>
            <li><a href="">Квест</a></li>
        </ul>
        <ul id="dropdown6" class="dropdown-content">
            <li><a href="">Персонажи</a></li>
            <li><a href="">Гарант</a></li>
            <li><a href="">Серебро</a></li>
            <li><a href="">Кидалы</a></li>
            <li><a href="">Инфо</a></li>
        </ul>
        <nav>
            <div class="nav-wrapper">
                <ul class="hide-on-med-and-down">
                    <li><a href="#">Монстры</a></li>
                    <li><a href="#">Материалы</a>
                    <li><a href="#">Обмундирование</a>
                    <li><a href="#">Сферы</a>
                    <li><a href="#">Руны</a>
                    <li><a href="#">Другое</a>
                    <li><a href="#">Услуги</a>
                    <!-- Dropdown Trigger -->
                    <li><a class="dropdown-trigger" href="#!" data-target="dropdown1">Dropdown<i class="material-icons right">arrow_drop_down</i></a></li>
                </ul>
            </div>
        </nav>
    ';
    bot();

?>

