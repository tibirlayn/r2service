<?php top('RF Online'); ?>

<!-- Dropdown Structure -->
<ul id="dropdown1" class="dropdown-content">
    <li><a href="">Производственные</a></li>
    <li><a href="">Свитки усиления</a></li>
    <li><a href="">Жезлы</a></li>
    <li><a href="">Книги</a></li>
    <li><a href="">Другое</a></li>
</ul>
<ul id="dropdown2" class="dropdown-content">
    <li><a href="">Оружие ближнего боя</a></li>
    <li><a href="">Оружие дальнего боя</a></li>
    <li><a href="">Доспехи</a></li>
    <li><a href="">Доп. защита</a></li>
    <li><a href="">Перчатки</a></li>
    <li><a href="">Сапоги</a></li>
    <li><a href="">Шлемы</a></li>
    <li><a href="">Плащи</a></li>
    <li><a href="">Аксессуары</a></li>
</ul>
<ul id="dropdown3" class="dropdown-content">
    <li><a href="">Сферы души</a></li>
    <li><a href="">Сферы жизни</a></li>
    <li><a href="">Сферы мастерства</a></li>
    <li><a href="">Сферы защиты</a></li>
    <li><a href="">Сферы разрушения</a></li>
    <li><a href="">Сферы характеристик</a></li>
</ul>
<ul id="dropdown4" class="dropdown-content">
    <li><a href="">Руны оружия</a></li>
    <li><a href="">Руны доспехов</a></li>
    <li><a href="">Руны шлемов</a></li>
    <li><a href="">Руны перчаток</a></li>
    <li><a href="">Руны сапог</a></li>
    <li><a href="">Руны плащей</a></li>
    <li><a href="">Руны браслетов/щитов</a></li>
    <li><a href="">Временные руны</a></li>
    <li><a href="">Сундуки с рунами</a></li>
</ul>
<ul id="dropdown5" class="dropdown-content">
    <li><a href="">Перевоплощения</a></li>
    <li><a href="">Питомец</a></li>
    <li><a href="">Квест</a></li>
</ul>
<ul id="dropdown6" class="dropdown-content">
    <li><a href="">Персонажи</a></li>
    <li><a href="">Гарант</a></li>
    <li><a href="">Серебро</a></li>
    <li><a href="">Кидалы</a></li>
    <li><a href="">Инфо</a></li>
</ul>
<nav>
    <div class="nav-wrapper light-blue lighten-2">
        <a href="http://tilann.ru" class="brand-logo">RF Online</a>
        <ul class="right hide-on-med-and-down">
            <li><a href="#">Монстры</a></li>
            <li><a href="#">Материалы</a>
            <li><a href="#">Обмундирование</a>
            <li><a href="#">Сферы</a>
            <li><a href="#">Руны</a>
            <li><a href="#">Другое</a>
            <li><a href="#">Услуги</a>
            <!-- Dropdown Trigger -->
            <li><a class="dropdown-trigger" href="#!" data-target="dropdown1">Dropdown<i class="material-icons right">arrow_drop_down</i></a></li>
        </ul>
    </div>
</nav>
<div class="row">
    <div class="col s12 m12 l12" id="main_img-r2"> <!-- Note that "m8 l9" was added -->
        <div class="col s12 m12 l12">
            <img class="responsive-img" src="/images/rf/main.jpg" alt="">
        </div>
        <div class="col s12 m12 l12">

            <div class="heading">
                <h5 class="left-align">ОБ ИГРЕ</h5>
                    <?php
                    $query_equipment = "SELECT 	description FROM `description` WHERE id_description = 2";
                    $data_equipment = mysqli_query($link, $query_equipment);
                    if (mysqli_num_rows($data_equipment) > 0) {
                        $row_equipment = mysqli_fetch_array($data_equipment);
                        echo '
                            <p>
                                '.$row_equipment["description"].'
                            </p>
                            ';
                    }
                    ?>
        </div>
            <div class="heading">
                <h5 class="left-align">ИГРОВЫЕ КЛАССЫ И РАСЫ</h5>

                            <?php
                            $query_equipment = "SELECT description.description_character, 
                                                game_image.image,
                                                game_image.image1, 
                                                game_image.image2, 
                                                game_image.image3, 
                                                game_image.image4 
                                                FROM `description`,`game_image`
                                                WHERE description.id_description = 2 AND game_image.id_image = 2";

                            $data_equipment = mysqli_query($link, $query_equipment);

                            if (mysqli_num_rows($data_equipment) > 0) {
                                $row_equipment = mysqli_fetch_array($data_equipment);
                                echo '
                                        <p>
                                            ' . $row_equipment["description_character"] . '
                                        </p>
                                        ';

                                            if ($row_equipment["image"] != "" && file_exists("images/rf/hero".$row_equipment["image"].".jpg") &&
                                                    $row_equipment["image1"] != "" && file_exists("images/rf/hero".$row_equipment["image1"].".jpg") &&
                                                        $row_equipment["image2"] != "" && file_exists("images/rf/hero".$row_equipment["image2"].".jpg")){

                                                $img_path = 'images/rf/hero'.$row_equipment["image"].".jpg";
                                                $img_path1 = 'images/rf/hero'.$row_equipment["image1"].".jpg";
                                                $img_path2 = 'images/rf/hero'.$row_equipment["image2"].".jpg";


                                            } else {
                                                $img_path = "/images/title/no-image.jpg";
                                            }
                                            echo '
                                                
                                                <div class="col s12 m4 l4 xl4">
                                                    <div class="card">
                                                        <div class="card-image">
                                                                <img  src="'.$img_path.'">
                                                        </div>
                                                        <div class="card-content center-align">
                                                            <p>Акретия</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col s12 m4 l4 xl4">
                                                    <div class="card">
                                                        <div class="card-image">
                                                                <img  src="'.$img_path2.'">
                                                        </div>
                                                        <div class="card-content center-align">
                                                            <p>Кора</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col s12 m4 l4 xl4">
                                                    <div class="card">
                                                        <div class="card-image">
                                                                <img  src="'.$img_path1.'">
                                                        </div>
                                                        <div class="card-content center-align">
                                                            <p>Беллато</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                </div>
                                                
                    ';
                            }
                            ?>
        </div>
            <?php
            $query_equipment = "SELECT description.status, 
                                description.platform, 
                                description.settling, 
                                description.developer, 
                                description.features,
                                genre.genre
                                FROM `description`, `genre` WHERE genre.id_genre = 1 AND description.id_description = 2";
            $data_equipment = mysqli_query($link, $query_equipment);
            if (mysqli_num_rows($data_equipment) > 0) {
                $row_equipment = mysqli_fetch_array($data_equipment);

            echo '

            <div class="heading">
                <h5 class="left-align">ДОП. ИНФОРМАЦИЯ</h5>
                <table class="highlight">
                    <tbody>
                    <tr>
                        <th>Жанр</th>
                        <td>'.$row_equipment["genre"].'</td>
                        <th>Сеттинг</th>
                        <td>'.$row_equipment["settling"].'</td>
                    </tr>
                    <tr>
                        <th>Стадия</th>
                        <td>'.$row_equipment["status"].'</td>
                        <th>Платформа</th>
                        <td>'.$row_equipment["platform"].'</td>
                    </tr>
                    <tr>
                        <th>Разработчик</th>
                        <td>'.$row_equipment["developer"].'</td>
                        <th>Особенности</th>
                        <td>'.$row_equipment["features"].'</td>
                    </tr>
                    </tbody>
                </table>
            </div>
            ';

            }
            ?>
      <div class="container">

        </div>
    </div>

</div>

<!--JavaScript at end of body for optimized loading-->
<script type="text/javascript" src="js/click.js"></script>


<?php bot(); ?>