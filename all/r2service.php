<?php
    define('ititilann', true);
?>
<!DOCTYPE html>
<html>
<head>
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>

    <link rel="shortcut icon" href="images/main/main.jpg" type="image/jpeg">
    <link rel="stylesheet" href="css/style.css">
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body>
<!-- Dropdown Structure -->
<ul id="dropdown1" class="dropdown-content">
    <li><a href="">Производственные</a></li>
    <li><a href="">Свитки усиления</a></li>
    <li><a href="">Жезлы</a></li>
    <li><a href="">Книги</a></li>
    <li><a href="">Другое</a></li>
</ul>
<ul id="dropdown2" class="dropdown-content">
    <li><a href="">Оружие ближнего боя</a></li>
    <li><a href="">Оружие дальнего боя</a></li>
    <li><a href="">Доспехи</a></li>
    <li><a href="">Доп. защита</a></li>
    <li><a href="">Перчатки</a></li>
    <li><a href="">Сапоги</a></li>
    <li><a href="">Шлемы</a></li>
    <li><a href="">Плащи</a></li>
    <li><a href="">Аксессуары</a></li>
</ul>
<ul id="dropdown3" class="dropdown-content">
    <li><a href="">Сферы души</a></li>
    <li><a href="">Сферы жизни</a></li>
    <li><a href="">Сферы мастерства</a></li>
    <li><a href="">Сферы защиты</a></li>
    <li><a href="">Сферы разрушения</a></li>
    <li><a href="">Сферы характеристик</a></li>
</ul>
<ul id="dropdown4" class="dropdown-content">
    <li><a href="">Руны оружия</a></li>
    <li><a href="">Руны доспехов</a></li>
    <li><a href="">Руны шлемов</a></li>
    <li><a href="">Руны перчаток</a></li>
    <li><a href="">Руны сапог</a></li>
    <li><a href="">Руны плащей</a></li>
    <li><a href="">Руны браслетов/щитов</a></li>
    <li><a href="">Временные руны</a></li>
    <li><a href="">Сундуки с рунами</a></li>
</ul>
<ul id="dropdown5" class="dropdown-content">
    <li><a href="">Перевоплощения</a></li>
    <li><a href="">Питомец</a></li>
    <li><a href="">Квест</a></li>
</ul>
<ul id="dropdown6" class="dropdown-content">
    <li><a href="">Персонажи</a></li>
    <li><a href="">Гарант</a></li>
    <li><a href="">Серебро</a></li>
    <li><a href="">Кидалы</a></li>
    <li><a href="">Инфо</a></li>
</ul>
<nav>
    <div class="nav-wrapper light-blue lighten-2">
        <a href="http://tilann.ru" class="brand-logo">R2 Online</a>
        <ul class="right hide-on-med-and-down">
            <li><a href="#">Монстры</a></li>
            <li><a href="#">Материалы</a>
            <li><a href="#">Обмундирование</a>
            <li><a href="#">Сферы</a>
            <li><a href="#">Руны</a>
            <li><a href="#">Другое</a>
            <li><a href="#">Услуги</a>
            <!-- Dropdown Trigger -->
            <li><a class="dropdown-trigger" href="#!" data-target="dropdown1">Dropdown<i class="material-icons right">arrow_drop_down</i></a></li>
        </ul>
    </div>
</nav>
<?php
    if(isset($_POST['send'])){
        include ("admin/connection.php");
        $link = mysqli_connect(con_localhost, con_user, con_password, con_db);

        $server = mysqli_real_escape_string($link, trim($_POST['server']));
        $class_person = mysqli_real_escape_string($link, trim($_POST['class_person']));
        $level_person = mysqli_real_escape_string($link, trim($_POST['level_person']));
        $pet = mysqli_real_escape_string($link, trim($_POST['pet']));
        $level_pet = mysqli_real_escape_string($link, trim($_POST['level_pet']));
        $fio = mysqli_real_escape_string($link, trim($_POST['fio']));
        $passport = mysqli_real_escape_string($link, trim($_POST['passport']));
        $kos = mysqli_real_escape_string($link, trim($_POST['kos']));
        $price = mysqli_real_escape_string($link, trim($_POST['price']));
        $contact = mysqli_real_escape_string($link, trim($_POST['contact']));
        $visible = 0;
        $name_user = mysqli_real_escape_string($link, trim($_POST['name_user']));
        $buy_sell = "Продам";
        $currency = mysqli_real_escape_string($link, trim($_POST['currency']));
        $mask = mysqli_real_escape_string($link, trim($_POST['mask']));
        $gold_crown = mysqli_real_escape_string($link, trim($_POST['gold_crown']));
        $stone_pet = mysqli_real_escape_string($link, trim($_POST['stone_pet']));
        $pet_grade = mysqli_real_escape_string($link, trim($_POST['pet_grade']));

        if($server == ''
            or $class_person == ''
            or $level_person == ''
            or $pet == ''
            or $level_pet == ''
            or $fio == ''
            or $passport == ''
            or $kos == ''
            or $price == ''
            or $contact == ''
            or $name_user == ''
            or $currency == ''
            or $mask == ''
            or $gold_crown == ''
            or $stone_pet == ''
            or $pet_grade == '') {
            echo 'error';
        }else
        {
            $query = "INSERT INTO `garant` (server, class, level, pet, level_pet, fio, pasport, kos, price, contact, visible, name_user, buy_sell, course, mask, gold_crown, stone_pet, pet_grade) 
             VALUES ('$server', '$class_person', '$level_person', '$pet', '$level_pet', '$fio', '$passport', '$kos', '$price', '$contact', '$visible', '$name_user', '$buy_sell', '$currency', '$mask', '$gold_crown', '$stone_pet', '$pet_grade')";

            mysqli_query($link, $query) or die("Ошибка " . mysqli_error($link));
            mysqli_close($link);
            exit();
        }

}
?>

<div class="col">
    <div class="container">
        <div class="row">
            <form id="service_form" action="r2service.php" method="POST">
            <div class="col s12 m12 l6 xl6">
                <div class="input-field col s12">
                    <select name="server">
                        <option value="" disabled selected>Сервер</option>
                        <option value="Югенес">Югенес</option>
                        <option value="Метеос">Метеос</option>
                        <option value="Arena">Arena</option>
                        <option value="Arena STORM">Arena STORM</option>
                        <option value="Bjorn">Bjorn</option>
                    </select>
                </div>
                <div class="input-field col s12">
                    <select name="class_person">
                        <option value="" disabled selected>Персонаж</option>
                        <option value="Рыцарь">Рыцарь</option>
                        <option value="Рейнджер">Рейнджер</option>
                        <option value="Маг">Маг</option>
                        <option value="Призыватель">Призыватель</option>
                        <option value="Ассасин">Ассасин</option>
                    </select>
                </div>
                <div class="input-field col s12">
                    <select name="fio">
                        <option value="" disabled selected>Ф.И.О.</option>
                        <option value="Не установленно">Не установленно</option>
                        <option value="Установленно, данные знаю">Установленно, данные знаю</option>
                        <option value="Установленно, данные не знаю">Установленно, данные не знаю</option>
                    </select>
                </div>
                <div class="input-field col s12">
                    <select name="passport">
                        <option value="" disabled selected>Паспорт</option>
                        <option value="Не установленно">Не установленно</option>
                        <option value="Установленно, данные знаю">Установленно, данные знаю</option>
                        <option value="Установленно, данные не знаю">Установленно, данные не знаю</option>
                    </select>
                </div>
                    <div class="input-field col s12">
                        <select name="kos">
                            <option value="" disabled selected>КОСЫ</option>
                            <option value="Нет">Нет</option>
                            <option value="Да (БИГВАР)">Да (БИГВАР)</option>
                            <option value="Да (НЕЙТРАЛ)">Да (НЕЙТРАЛ)</option>
                            <option value="Да (ЛИЧНЫЙ)">Да (ЛИЧНЫЙ)</option>
                        </select>
                    </div>
                <div class="input-field col s12">
                    <input name="level_person" id="level_person_dd" type="text" class="validate" >
                    <label id="level_person_dd" for="level_person_dd">Уровень персонажа</label>
                    <span class="helper-text"><label id="level_person-error" class="error" for="level_person_dd"></label></span>
                </div>
                <div class="input-field col s12">
                    <input name="mask" id="mask_add" type="text" class="validate" >
                    <label for="mask_add">Трофей: Маска</label>
                    <span class="helper-text"><label id="mask-error" class="error" for="mask_add"></label></span>
                </div>
                <div class="input-field col s12">
                    <input name="gold_crown" id="gold_crown_add" type="text" class="validate">
                    <label for="gold_crown_add">Трофей: Золотая корона</label>
                    <span class="helper-text"><label id="gold_crown-error" class="error" for="gold_crown_add"></label></span>
                </div>
            </div>

            <div class="col s12 m12 l6 xl6">
                <div class="input-field col s12">
                    <select name="pet" id="pet">
                        <option value="" disabled selected>Питомец</option>
                        <option value="Нету питомца">Нету питомца</option>
                        <option value="Огня" data-icon="images/r2/1756.jpg">Огня</option>
                        <option value="Воды" data-icon="images/r2/1757.jpg">Воды</option>
                        <option value="Земли" data-icon="images/r2/1765.jpg">Земли</option>
                        <option value="Луны" data-icon="images/r2/1759.jpg">Луны</option>
                        <option value="Солнца" data-icon="images/r2/1760.jpg">Солнца</option>
                        <option value="Ветра" data-icon="images/r2/1773.jpg">Ветра</option>
                    </select>
                </div>
                <div class="input-field col s12">
                    <select name="stone_pet" id="stone_pet">
                        <option value="" disabled selected>Камень питомеца</option>
                        <option value="Нету питомца" >Нету питомца</option>
                        <option value="Обычный камень" data-icon="images/r2/1756.jpg">Обычный камень</option>
                        <option value="Сияющий камень" data-icon="images/r2/1761.jpg">Сияющий камень</option>
                        <option value="Блестящий камень" data-icon="images/r2/1744.jpg">Блестящий камень</option>
                    </select>
                </div>
                <div class="input-field col s12">
                    <select name="pet_grade" id="pet_grade">
                        <option value="" disabled selected>Специализация Питомеца</option>
                        <option value="Нету питомца">Нету питомца</option>
                        <option value="Максимум HP" data-icon="images/r2/2043.png">Максимум HP</option>
                        <option value="Восстановление HP" data-icon="images/r2/2061.png">Восстановление HP</option>
                        <option value="Максимум MP" data-icon="images/r2/2049.png">Максимум MP</option>
                        <option value="Восстановление MP" data-icon="images/r2/2064.png">Восстановление MP</option>
                        <option value="Максимум HP/MP" data-icon="images/r2/2055.png">Максимум HP/MP</option>
                        <option value="Восстановление HP/MP" data-icon="images/r2/2073.png">Восстановление HP/MP</option>
                        <option value="Вес" data-icon="images/r2/2076.png">Вес</option>
                        <option value="Время перевоплощения" data-icon="images/r2/2058.png">Время перевоплощения</option>
                        <option value="HP/вес" data-icon="images/r2/2052.png">HP/вес</option>
                        <option value="Восстановление" data-icon="images/r2/2070.png">Восстановление</option>
                        <option value="MP/Вес" data-icon="images/r2/2046.png">MP/Вес</option>
                        <option value="Восстановление" data-icon="images/r2/2067.png">Восстановление</option>
                    </select>
                </div>

                <div class="input-field col s12">
                    <input name="level_pet" id="level_pet_add" type="text" class="validate">
                    <label for="level_pet_add">Уровень питомца</label>
                    <span class="helper-text"><label id="level_pet-error" class="error" for="level_pet_add"></label></span>
                </div>
                <div class="input-field col s12">
                    <select name="currency" >
                        <option value="" disabled selected>Валюта</option>
                        <option value="Рубль">Рубли</option>
                        <option value="Серебро">Серебро</option>
                    </select>
                </div>
                <div class="input-field col s12">
                    <input name="price" id="price_add" type="text" class="validate" >
                    <label for="price_add">Цена</label>
                    <span class="helper-text"><label id="price-error" class="error" for="price_add"></label></span>
                </div>
                <div class="input-field col s12">
                    <input name="contact" id="contact" type="text" class="validate" maxlength="55" >
                    <label for="contact">Моя страница</label>
                    <span class="helper-text"><label id="contact-error" class="error" for="contact"></label></span>
                </div>
                <div class="input-field col s12">
                    <input name="name_user" id="name_user" type="text" class="validate" maxlength="55" >
                    <label for="name_user">Имя страницы</label>
                    <span class="helper-text"><label id="name_user-error" class="error" for="name_user"></label></span>
                </div>
                <div class="center-align">
                    <button class="btn waves-effect waves-light btn-large pulse" type="submit" name="send">ОТПРАВИТЬ
                        <i class="material-icons right">send</i>
                    </button>
                </div>
            </div>
            </form>
        </div>

    </div>
</div>
<?php
    include ("footer.php");
?>
<!--JavaScript at end of body for optimized loading-->
<script type="text/javascript" src="js/click.js"></script>
<script type="text/javascript" src="js/jquery.formatter.min.js"></script>
<script type="text/javascript" src="js/jquery.validate.min.js"></script>
<script type="text/javascript" src="js/jquery.mask.min.js"></script>
<script>
    $(document).ready(function () {
        $('.dropdown-trigger').dropdown();
        $('select').formSelect();

        $('#level_person_dd').formatter({
            'pattern': '{{999}}'
        });

        $('#gold_crown_add').formatter({
            'pattern': '{{999}}'
        });

        $('#mask_add').formatter({
            'pattern': '{{999}}'
        });

        $('#level_pet_add').formatter({
            'pattern': '{{99}}'
        });

        $('#service_form').validate({
           rules:{
               server:{
                   required: true
               },
               class_person: {
                   required: true
               },
               fio: {
                   required: true
               },
               passport: {
                   required: true
               },
               kos: {
                   required: true
               },
               level_person: {
                   required: true,
                   max: 200
               },
               mask: {
                   required: true,
                   max: 950
               },
               gold_crown:{
                   required: true,
                   max: 950
               },
               pet: {
                   required: true
               },
               stone_pet: {
                   required: true
               },
               pet_grade: {
                   required: true
               },
               level_pet: {
                   required: true,
                   max: 90
               },
               currency: {
                   required: true
               },
               price: {
                   required: true
               },
               contact: {
                   required: true,
                   url: true
               },
               name_user:{
                   required: true,
                   maxlength: 55
               }
           },
            messages: {
                server:{
                    required: "Поле 'Сервер' обязательно к заполнению"
                },
                class_person: {
                    required: "Поле 'Персонаж' обязательно к заполнению"
                },
                fio: {
                    required: "Поле 'Ф.И.О.' обязательно к заполнению"
                },
                passport: {
                    required: "Поле 'Паспорт' обязательно к заполнению"
                },
                kos: {
                    required: "Поле 'КОСЫ' обязательно к заполнению"
                },
                level_person: {
                    required: "Поле 'Уровеь персонажа' обязательно к заполнению",
                    max: "Введите значение, меньшее или равное 200."
                },
                mask: {
                    required: "Поле 'Трофей: Маска' обязательно к заполнению",
                    max: "Введите значение, меньшее или равное 950."
                },
                gold_crown:{
                    required: "Поле 'Трофей: Золотая корона' обязательно к заполнению",
                    max: "Введите значение, меньшее или равное 950."
                },
                pet: {
                    required: "Поле 'Питомец' обязательно к заполнению"
                },
                stone_pet: {
                    required: "Поле 'Камень питомца' обязательно к заполнению"
                },
                pet_grade: {
                    required: "Поле 'Питомец грейд' обязательно к заполнению"
                },
                level_pet: {
                    required: "Поле 'Уровень питомца' обязательно к заполнению",
                    max: "Введите значение, меньшее или равное 90."
                },
                currency: {
                    required: "Поле 'Валюта' обязательно к заполнению"
                },
                price: {
                    required: "Поле 'Цена' обязательно к заполнению"
                },
                contact: {
                    required: "Поле 'Моя страница' обязательно к заполнению",
                    url: "Пожалуйста, введите корректный адрес."
            },
                name_user:{
                    required: "Поле 'Имя страницы' обязательно к заполнению",
                    maxlength: "Введите не больше 55-ти символов в поле 'Repeat password'"
                }
            }
        });

        $('#price_add').mask('000.000.000.000.000', {reverse: true});

    });
</script>
</body>
</html>
