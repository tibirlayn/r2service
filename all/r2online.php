<?php top('R2 Online'); ?>

<ul id="dropdown1" class="dropdown-content">
    <li><a href="">Производственные</a></li>
    <li><a href="">Свитки усиления</a></li>
    <li><a href="">Жезлы</a></li>
    <li><a href="">Книги</a></li>
    <li><a href="">Другое</a></li>
</ul>
<ul id="dropdown2" class="dropdown-content">
    <li><a href="">Оружие ближнего боя</a></li>
    <li><a href="">Оружие дальнего боя</a></li>
    <li><a href="">Доспехи</a></li>
    <li><a href="">Доп. защита</a></li>
    <li><a href="">Перчатки</a></li>
    <li><a href="">Сапоги</a></li>
    <li><a href="">Шлемы</a></li>
    <li><a href="">Плащи</a></li>
    <li><a href="">Аксессуары</a></li>
</ul>
<ul id="dropdown3" class="dropdown-content">
    <li><a href="">Сферы души</a></li>
    <li><a href="">Сферы жизни</a></li>
    <li><a href="">Сферы мастерства</a></li>
    <li><a href="">Сферы защиты</a></li>
    <li><a href="">Сферы разрушения</a></li>
    <li><a href="">Сферы характеристик</a></li>
</ul>
<ul id="dropdown4" class="dropdown-content">
    <li><a href="">Руны оружия</a></li>
    <li><a href="">Руны доспехов</a></li>
    <li><a href="">Руны шлемов</a></li>
    <li><a href="">Руны перчаток</a></li>
    <li><a href="">Руны сапог</a></li>
    <li><a href="">Руны плащей</a></li>
    <li><a href="">Руны браслетов/щитов</a></li>
    <li><a href="">Временные руны</a></li>
    <li><a href="">Сундуки с рунами</a></li>
</ul>
<ul id="dropdown5" class="dropdown-content">
    <li><a href="">Перевоплощения</a></li>
    <li><a href="">Питомец</a></li>
    <li><a href="">Квест</a></li>
</ul>
<ul id="dropdown6" class="dropdown-content">
    <li><a href="">Персонажи</a></li>
    <li><a href="">Гарант</a></li>
    <li><a href="">Серебро</a></li>
    <li><a href="">Кидалы</a></li>
    <li><a href="">Инфо</a></li>
</ul>
<nav>
    <div class="nav-wrapper light-blue lighten-2">
        <a href="http://tilann.ru" class="brand-logo">R2 Online</a>
        <ul class="right hide-on-med-and-down">
            <li><a href="#">Монстры</a></li>
            <li><a href="#">Материалы</a>
            <li><a href="#">Обмундирование</a>
            <li><a href="#">Сферы</a>
            <li><a href="#">Руны</a>
            <li><a href="#">Другое</a>
            <li><a href="#">Услуги</a>
            <!-- Dropdown Trigger -->
            <li><a class="dropdown-trigger" href="#!" data-target="dropdown1">Dropdown<i class="material-icons right">arrow_drop_down</i></a></li>
        </ul>
    </div>
</nav>
<div class="row">
    <div class="col l3 hide-on-med-and-down"> <!-- Note that "m4 l3" was added -->

        <ul id="accordion" class="accordion">
            <li>
                <div class="link">БАШНЯ МЕТЕОСА<i class="large material-icons">expand_more</i></div>
                <ul class="submenu">
                    <li><a href="#">БАШНЯ МЕТЕОСА 1 УР (ЗЕМЛЯ)</a></li>
                    <li><a href="#">БАШНЯ МЕТЕОСА 2 УР (ВОЗДУХ)</a></li>
                    <li><a href="#">БАШНЯ МЕТЕОСА 3 УР (ОКЕАН)</a></li>
                    <li><a href="#">БАШНЯ МЕТЕОСА 4 УР (СОЛНЦЕ)</a></li>
                </ul>
            </li>
            <li>
                <div class="link">БАШНЯ ОГНЯ<i class="large material-icons">expand_more</i></div>
                <ul class="submenu">
                    <li><a href="">БАШНЯ ОГНЯ 1 УР</a></li>
                    <li><a href="">БАШНЯ ОГНЯ 2 УР</a></li>
                    <li><a href="">БАШНЯ ОГНЯ 3 УР</a></li>
                    <li><a href="">БАШНЯ ОГНЯ 4 УР</a></li>
                    <li><a href="">БАШНЯ ОГНЯ 5 УР</a></li>
                </ul>
            </li>
            <li>
                <div class="link">БЕЗЫМЯННАЯ ПЕЩЕРА<i class="large material-icons">expand_more</i></div>
                <ul class="submenu">
                    <li><a href="">БЕЗЫМЯННАЯ ПЕЩЕРА 1 УР</a></li>
                    <li><a href="">БЕЗЫМЯННАЯ ПЕЩЕРА 2 УР</a></li>
                </ul>
            </li>
            <li>
                <div class="link">БОЛОТО ЧЕРНОГО ДРАКОНА<i class="large material-icons">expand_more</i></div>
                <ul class="submenu">
                    <li><a href="">БОЛОТО ЧЕРНОГО ДРАКОНА 1 УР</a></li>
                    <li><a href="">БОЛОТО ЧЕРНОГО ДРАКОНА 2 УР</a></li>
                    <li><a href="">БОЛОТО ЧЕРНОГО ДРАКОНА 3 УР</a></li>
                    <li><a href="">БОЛОТО ЧЕРНОГО ДРАКОНА 4 УР</a></li>
                    <li><a href="">БОЛОТО ЧЕРНОГО ДРАКОНА 5 УР</a></li>
                </ul>
            </li>
            <li>
                <div class="link">ГОРИЗОНТ ПОЛНОЙ ЛУНЫ</div>
            </li>
            <li>
                <div class="link">ГРОБНИЦА КОРОЛЯ<i class="large material-icons">expand_more</i></div>
                <ul class="submenu">
                    <li><a href="">ГРОБНИЦА КОРОЛЯ 1 УР</a></li>
                    <li><a href="">ГРОБНИЦА КОРОЛЯ 2 УР</a></li>
                    <li><a href="">ГРОБНИЦА КОРОЛЯ 3 УР</a></li>
                    <li><a href="">ГРОБНИЦА КОРОЛЯ 4 УР</a></li>
                </ul>
            </li>
            <li>
                <div class="link">ЗАЛ ИСПЫТАНИЙ</div>
            </li>
            <li>
                <div class="link">КРЕПОСТЬ АБЕЛЛУСА</div>
            </li>
            <li>
                <div class="link">ЛЕСТНИЦА ДРЕВНИХ<i class="large material-icons">expand_more</i></div>
                <ul class="submenu">
                    <li><a href="">ЛЕСТНИЦА ДРЕВНИХ	1 УР</a></li>
                    <li><a href="">ЛЕСТНИЦА ДРЕВНИХ	2 УР</a></li>
                    <li><a href="">ЛЕСТНИЦА ДРЕВНИХ 3 УР</a></li>
                </ul>
            </li>
            <li>
                <div class="link">ЛОГОВО ЖУКОВ<i class="large material-icons">expand_more</i></div>
                <ul class="submenu">
                    <li><a href="">ЛОГОВО ЖУКОВ 1 УР</a></li>
                    <li><a href="">ЛОГОВО ЖУКОВ 2 УР</a></li>
                    <li><a href="">ЛОГОВО ЖУКОВ 3 УР</a></li>
                </ul>
            </li>
            <li>
                <div class="link">НЕБЕСНЫЙ ОСТРОВ ЭЛЬТЕР</div>
            </li>
            <li>
                <div class="link">ОЗЕРО ПОЛНОЙ ЛУНЫ</div>
            </li>
            <li>
                <div class="link">ОСТРОВ АКРА</div>
            </li>
            <li>
                <div class="link">ОСТРОВ ГВИНЕЯ</div>
            </li>
            <li>
                <div class="link">ОСТРОВ КОЛФОРТ</div>
            </li>
            <li>
                <div class="link">ПЕЩЕРА ДРЕВНИХ ПИРАТОВ<i class="large material-icons">expand_more</i></div>
                <ul class="submenu">
                    <li><a href="">ПЕЩЕРА ДРЕВНИХ ПИРАТОВ 1 УР</a></li>
                    <li><a href="">ПЕЩЕРА ДРЕВНИХ ПИРАТОВ 2 УР</a></li>
                </ul>
            </li>
            <li>
                <div class="link">ПЕЩЕРА ЭГИРА<i class="large material-icons">expand_more</i></div>
                <ul class="submenu">
                    <li><a href="">ПЕЩЕРА ЭГИРА 1 УР</a></li>
                    <li><a href="">ПЕЩЕРА ЭГИРА 2 УР</a></li>
                    <li><a href="">ПЕЩЕРА ЭГИРА 3 УР</a></li>
                </ul>
            </li>
            <li>
                <div class="link">ПОДЗЕМЕЛЬЕ БААЛЬБЕКА<i class="large material-icons">expand_more</i></div>
                <ul class="submenu">
                    <li><a href="">ПОДЗЕМЕЛЬЕ БААЛЬБЕКА 1 УР</a></li>
                    <li><a href="">ПОДЗЕМЕЛЬЕ БААЛЬБЕКА 2 УР</a></li>
                </ul>
            </li>
            <li>
                <div class="link">ПОДЗЕМЕЛЬЕ ВАЛЛЕФОРА<i class="large material-icons">expand_more</i></div>
                <ul class="submenu">
                    <li><a href="">ПОДЗЕМЕЛЬЕ ВАЛЛЕФОРА 1 УР</a></li>
                    <li><a href="">ПОДЗЕМЕЛЬЕ ВАЛЛЕФОРА 2 УР</a></li>
                    <li><a href="">ПОДЗЕМЕЛЬЕ ВАЛЛЕФОРА 3 УР</a></li>
                </ul>
            </li>
            <li>
                <div class="link">ПОДЗЕМЕЛЬЕ НЕЖИТИ<i class="large material-icons">expand_more</i></div>
                <ul class="submenu">
                    <li><a href="">ПОДЗЕМЕЛЬЕ НЕЖИТИ 1 УР</a></li>
                    <li><a href="">ПОДЗЕМЕЛЬЕ НЕЖИТИ 2 УР</a></li>
                    <li><a href="">ПОДЗЕМЕЛЬЕ НЕЖИТИ 3 УР</a></li>
                    <li><a href="">ПОДЗЕМЕЛЬЕ НЕЖИТИ 4 УР</a></li>
                </ul>
            </li>
            <li>
                <div class="link">ПОДЗЕМЕЛЬЕ ЮПИТЕРА<i class="large material-icons">expand_more</i></div>
                <ul class="submenu">
                    <li><a href="">ПОДЗЕМЕЛЬЕ ЮПИТЕРА 1 УР</a></li>
                    <li><a href="">ПОДЗЕМЕЛЬЕ ЮПИТЕРА 2 УР</a></li>
                </ul>
            </li>
            <li>
                <div class="link">ПРЕМИУМ ПОДЗЕМЕЛЬЕ<i class="large material-icons">expand_more</i></div>
                <ul class="submenu">
                    <li><a href="">ПРЕМИУМ ПОДЗЕМЕЛЬЕ 1 УР</a></li>
                    <li><a href="">ПРЕМИУМ ПОДЗЕМЕЛЬЕ 2 УР</a></li>
                </ul>
            </li>
            <li>
                <div class="link">РУИНЫ ПОЛНОЛУНИЯ</div>
            </li>
            <li>
                <div class="link">СВЯТЫЕ ЗЕМЛИ ИЛЛЮМИНА</div>
            </li>
            <li>
                <div class="link">ТЕМНЫЕ ПЕЩЕРЫ<i class="large material-icons">expand_more</i></div>
                <ul class="submenu">
                    <li><a href="">ТЕМНЫЕ ПЕЩЕРЫ 1 УР</a></li>
                    <li><a href="">ТЕМНЫЕ ПЕЩЕРЫ 2 УР</a></li>
                    <li><a href="">ТЕМНЫЕ ПЕЩЕРЫ 3 УР</a></li>
                    <li><a href="">ТЕМНЫЕ ПЕЩЕРЫ 4 УР</a></li>
                    <li><a href="">ТЕМНЫЕ ПЕЩЕРЫ 5 УР</a></li>
                </ul>
            </li>
            <li>
                <div class="link">ХРАМ МЕТЕОСА<i class="large material-icons">expand_more</i></div>
                <ul class="submenu">
                    <li><a href="">ХРАМ МЕТЕОСА 1 УР</a></li>
                    <li><a href="">ХРАМ МЕТЕОСА 2 УР</a></li>
                </ul>
            </li>
        </ul>
    </div>

    <div class="col s12 m12 l9" id="main_img-r2"> <!-- Note that "m8 l9" was added -->
        <div class="col s12 m12 l12">
            <img class="responsive-img" src="/images/r2/main.jpg" alt="">
        </div>
        <div class="col s12 m12 l12">

            <div class="heading">
                <h5 class="left-align">ОБ ИГРЕ</h5>

                <?php
                //include ("/admin/connection_info.php");
                //$link = mysqli_connect(con_localhost, con_user, con_password, con_db);
                $query_equipment = "SELECT description FROM `description`";
                $data_equipment = mysqli_query($link, $query_equipment);
                if (mysqli_num_rows($data_equipment) > 0) {
                    $row_equipment = mysqli_fetch_array($data_equipment);
                    echo '
                            <p>
                                '.$row_equipment["description"].'
                            </p>
                            ';
                }
                ?>
           </div>
            <div class="heading">
                <h5 class="left-align">ИГРОВЫЕ КЛАССЫ</h5>
                <?php
                $query_equipment = "SELECT description.description_character, 
                                                game_image.image,
                                                game_image.image1, 
                                                game_image.image2, 
                                                game_image.image3, 
                                                game_image.image4 
                                                FROM `description`,`game_image`
                                                WHERE description.id_image = game_image.image_id
                                                ORDER BY game_image.image_id";

                $data_equipment = mysqli_query($link, $query_equipment);
                if (mysqli_num_rows($data_equipment) > 0) {
                    $row_equipment = mysqli_fetch_array($data_equipment);
                    echo '
                                        <p>
                                            ' . $row_equipment["description_character"] . '
                                        </p>
                                        ';

                    if ($row_equipment["image"] != "" && file_exists("images/r2/hero".$row_equipment["image"].".png") &&
                        $row_equipment["image1"] != "" && file_exists("images/r2/hero".$row_equipment["image1"].".png") &&
                        $row_equipment["image2"] != "" && file_exists("images/r2/hero".$row_equipment["image2"].".png") &&
                        $row_equipment["image3"] != "" && file_exists("images/r2/hero".$row_equipment["image3"].".png") &&
                        $row_equipment["image4"] != "" && file_exists("images/r2/hero".$row_equipment["image4"].".png")){

                        $img_path = 'images/r2/hero'.$row_equipment["image"].".png";
                        $img_path1 = 'images/r2/hero'.$row_equipment["image1"].".png";
                        $img_path2 = 'images/r2/hero'.$row_equipment["image2"].".png";
                        $img_path3 = 'images/r2/hero'.$row_equipment["image3"].".png";
                        $img_path4 = 'images/r2/hero'.$row_equipment["image4"].".png";

                    } else {
                        $img_path = "/images/title/no-image.jpg";
                    }
                    echo '
                                                
                                                <div class="col s12 m2 l2 xl2">
                                                    <div class="card">
                                                        <div class="card-image">
                                                                <img  src="'.$img_path.'">
                                                        </div>
                                                        <div class="card-content center-align">
                                                            <p>Маг</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col s12 m2 l2 xl2">
                                                    <div class="card">
                                                        <div class="card-image">
                                                                <img  src="'.$img_path1.'">
                                                        </div>
                                                        <div class="card-content center-align">
                                                            <p>Ассасин</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col s12 m2 l2 xl2">
                                                    <div class="card">
                                                        <div class="card-image">
                                                                <img  src="'.$img_path2.'">
                                                        </div>
                                                        <div class="card-content center-align">
                                                            <p>Рыцарь</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                <div class="col s12 m3 l3 xl3">
                                                    <div class="card">
                                                        <div class="card-image">
                                                                <img src="'.$img_path4.'">
                                                        </div>
                                                        <div class="card-content center-align">
                                                            <p>Призыватель</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                <div class="col s12 m3 l3 xl3">
                                                    <div class="card">
                                                        <div class="card-image">
                                                                <img src="'.$img_path3.'">
                                                        </div>
                                                        <div class="card-content center-align">
                                                            <p>Рейнджер</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                </div>
                                                
                    ';
                }
                ?>
        </div>
            <?php
            $query_equipment = "SELECT description.status, 
                                description.platform, 
                                description.settling, 
                                description.developer, 
                                description.features,
                                genre.genre
                                FROM `description`, `genre` WHERE genre.id_genre = 1";
            $data_equipment = mysqli_query($link, $query_equipment);
            if (mysqli_num_rows($data_equipment) > 0) {
                $row_equipment = mysqli_fetch_array($data_equipment);

                echo '

            <div class="heading">
                <h5 class="left-align">ДОП. ИНФОРМАЦИЯ</h5>
                <table class="highlight">
                    <tbody>
                    <tr>
                        <th>Жанр</th>
                        <td>'.$row_equipment["genre"].'</td>
                        <th>Сеттинг</th>
                        <td>'.$row_equipment["settling"].'</td>
                    </tr>
                    <tr>
                        <th>Стадия</th>
                        <td>'.$row_equipment["status"].'</td>
                        <th>Платформа</th>
                        <td>'.$row_equipment["platform"].'</td>
                    </tr>
                    <tr>
                        <th>Разработчик</th>
                        <td>'.$row_equipment["developer"].'</td>
                        <th>Особенности</th>
                        <td>'.$row_equipment["features"].'</td>
                    </tr>
                    </tbody>
                </table>
            </div>
            ';

            }
            ?>

            <div class="container">

            </div>
        </div>

    </div>

    <!--JavaScript at end of body for optimized loading-->
    <script type="text/javascript" src="/js/click.js"></script>
    <script type="text/javascript" src="/js/initialization.js"></script>


<?php bot(); ?>