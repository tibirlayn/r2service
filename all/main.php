<?php
top('Main');
?>
<nav>
        <div class="nav-wrapper light-blue lighten-2">
            <div class="container">
                <div class="left">
                    <a href="http://tilann.ru/" class="brand-logo center">Tilann</a>
                    <ul id="nav-mobile" class="right hide-on-med-and-down">
                    </ul>
                </div>
            </div>
    </div>
</nav>
<!--slider-->
<div class="slider hide-on-med-and-down">
    <ul class="slides">
        <li>
            <img src="/images/slider/slider1.jpg"> <!-- random image -->
            <div class="caption center-align">
                <h3>R2 Online</h3>
                <h5 class="light grey-text text-lighten-3">Игра по твоим правилам.</h5>
            </div>
        </li>
        <li>
            <img src="/images/slider/slider2.jpg"> <!-- random image -->
            <div class="caption center-align">
                <h3>Lineage 2</h3>
                <h5 class="light grey-text text-lighten-3">Найди свою судьбу.</h5>
            </div>
        </li>
        <li>
            <img src="/images/slider/slider3.jpg"> <!-- random image -->
            <div class="caption left-align">
                <h3>Revalution</h3>
                <h5 class="light grey-text text-lighten-3">Играй и побеждай красиво.</h5>
            </div>
        </li>
        <li>
            <img src="/images/slider/slider4.jpg"> <!-- random image -->
            <div class="caption left-align">
                <h3>World of Warcraft</h3>
                <h5 class="light grey-text text-lighten-3">Cила превыше всего.</h5>
            </div>
        </li>
        <li>
            <img src="/images/slider/slider5.jpg"> <!-- random image -->
            <div class="caption left-align">
                <h3>Black Desert</h3>
                <h5 class="light grey-text text-lighten-3">Разделяй и властвуй.</h5>
            </div>
        </li>
    </ul>
</div>

<!--main-->
<div class="container">
    <div class="heading">
        <h5 class="left-align">TOP MMORPG</h5>
    </div>
    <div class="row">
        <?php
        $link = mysqli_connect('localhost', 'tilann_r2service', '756SAd179wte', 'tilann_r2service') or die("Ошибка " . mysqli_error($link));
        $query_equipment = "SELECT * FROM `game` WHERE visible = 1 AND id_genre = 1";
        $data_equipment = mysqli_query($link, $query_equipment);

        if (mysqli_num_rows($data_equipment) > 0){
            $row_equipment = mysqli_fetch_array($data_equipment);
            do{

                if ($row_equipment["image"] != "" && file_exists("images/title/".$row_equipment["image"].".jpg")){
                    $img_path = 'images/title/'.$row_equipment["image"].".jpg";
                    $link_game = $row_equipment["link"];
                } else {
                    $img_path = "/images/title/no-image.jpg";
                }
                echo '
                            <div class="col s12 m6 l6 xl4">
                                <div class="card hoverable">
                                    <div class="card-image">
                                        <a href="'.$link_game.'">
                                            <img class="img_title" src="'.$img_path.'">
                                        </a>
                                    </div>
                                    <div class="card-content center-align">
                                        <p>'.$row_equipment["name"].'</p>
                                    </div>
                                </div>
                            </div>
                        ';
            }
            while ($row_equipment = mysqli_fetch_array($data_equipment));
        }
        ?>
   </div>
</div>

<div class="container">
    <div class="heading">
        <h5 class="left-align">TOP Action</h5>
    </div>
    <div class="row">

        <?php
        $query_equipment = "SELECT * FROM `game` WHERE visible = 1 AND id_genre = 2";
        $data_equipment = mysqli_query($link, $query_equipment);

        if (mysqli_num_rows($data_equipment) > 0){
            $row_equipment = mysqli_fetch_array($data_equipment);
            do{

                if ($row_equipment["image"] != "" && file_exists("images/title/".$row_equipment["image"].".jpg")){
                    $img_path = 'images/title/'.$row_equipment["image"].".jpg";
                    $link_game = $row_equipment["link"];
                } else {
                    $img_path = "/images/title/no-image.jpg";
                }
                echo '
                            <div class="col s12 m6 l6 xl4">
                                <div class="card hoverable">
                                    <div class="card-image">
                                        <a href="'.$link_game.'">
                                            <img class="img_title" src="'.$img_path.'">
                                        </a>
                                    </div>
                                    <div class="card-content center-align">
                                        <p>'.$row_equipment["name"].'</p>
                                    </div>
                                </div>
                            </div>
                        ';
            }
            while ($row_equipment = mysqli_fetch_array($data_equipment));
        }
        ?>
    </div>
</div>

<script type="text/javascript" src="/js/carousel.js"></script>

<?php bot();?>
