    function post_query( url, name, data ) {

    var str = '';

        $.each( data.split('.'), function ( k, v ) {
            str += '&' + v + '=' + $('#' + v).val();
        } );
        $.ajax({
            type: 'POST',
            url: '/' + url,
            data: name + '_f=1' + str,
            cache: false,
            success: function ( result ) {
                obj = jQuery.parseJSON( result );

                if ( obj.go ){
                    goLocation( obj.go );
                }
                else {
                    alert( obj.message );
                }
            }
        });
    }

    function goLocation( url ) {
        window.location.href = '/' + url;
    }