$(function () {
    $('.error').hide();
    $(".button").submit(function () {

        $('.error').hide();
        var name = new $("input#name").val();
        if (name == "" || name.length < 6){
            $("label#name_error").show();
            $("input#name").focus();
            return false;
        }

        var login = new $("input#login").val();
        if (login == "" || login.length < 6){
            $("label#login_error").show();
            $("input#login").focus();
            return false;
        }

        var email = new $("input#email").val();
        if (email == ""){
            $("label#email_error").show();
            $("input#email").focus();
            return false;
        }

        var password = new $("input#password").val();
        if (password == "" || password.length < 8){
            $("label#password_error").show();
            $("input#password").focus();
            return false;
        }

        var r_password = new $("input#r_password").val();
        if (r_password == "" || r_password.length < 8){
            $("label#r_password").show();
            $("input#r_password").focus();
            return false;
        }

        if (password != r_password) {
            $("label#password").show();
            $("label#r_password").show();
            $("input#password").focus();
            $("input#r_password").val("");
            return false;
        }

        var dataString = 'name=' + name + 'login=' + login + 'email=' + email + 'password' + password;

        $.ajax()({
            type: "POST",
            url: "aith_registration.php",
            data: dataString,
            dataType: "html",
            success: registration
        });
    });
});