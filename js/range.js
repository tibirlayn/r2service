$(document).ready(function () {
    var range_level = document.getElementById('range_level');

    noUiSlider.create(range_level, {
        start: [50, 150],
        connect: true,
        range: {
            'min': 0,
            'max': 200
        }
    });

    var range_trophy_mask = document.getElementById('range_trophy_mask');

    noUiSlider.create(range_trophy_mask, {
        start: [220, 620],
        connect: true,
        range: {
            'min': 0,
            'max': 880
        }
    });


    var range_trophy_zk = document.getElementById('range_trophy_zk');

    noUiSlider.create(range_trophy_zk, {
        start: [220, 620],
        connect: true,
        range: {
            'min': 0,
            'max': 880
        }
    });

    var range_price = document.getElementById('range_price');

    noUiSlider.create(range_price, {
        start: [249999999999, 749999999998],
        connect: true,
        range: {
            'min': 0,
            'max': 999999999999
        }
    });
});