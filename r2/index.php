<?php
	include ("admin/connection_info.php");
	$link = mysqli_connect(con_localhost, con_user, con_password, con_db);
?>
<!DOCTYPE html>
<html lang="ru">
        <head>
			<!-- Required meta tags -->
			<meta charset="utf-8">
			<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

			<title>R2Servicedb</title>

			<!-- Bootstrap CSS -->
			<!--<link rel="stylesheet" type="text/css" href="css/bootstrap.css">﻿-->
            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
			<link rel="shortcut icon" href="images/main_title.jpg" type="image/jpeg">
			<link rel="stylesheet" href="css/reset.css">
			<link rel="stylesheet" href="css/index.css">

		</head>
        <body>

		<?php
			include ("leftmenu.php");
            include ("navmenu.php");
		?>

		<div class="top_field">
			<div class="top_info">

			</div>
			<div class="bottom_info">
				<div class="div_bottom_info">

				</div>
				<div class="div_bottom_info">
					<input type="text" pattern="[A-Za-z]+$" maxlength="25" placeholder="Поиск"/>
				</div>
			</div>
		</div>

		<div class="main_menu">
			<?php
			$query_equipment = "SELECT * FROM `object` WHERE visible = 1";
			$data_equipment = mysqli_query($link, $query_equipment);

			if (mysqli_num_rows($data_equipment) > 0){
				$row_equipment = mysqli_fetch_array($data_equipment);
				do{

					if ($row_equipment["image"] != "" && file_exists("images/r2_icon/".$row_equipment["image"].".jpg")){

						$img_path = 'images/r2_icon/'.$row_equipment["image"].".jpg";
						$max_width = 48;
						$max_height = 48;
						list($width, $height) = getimagesize($img_path);
						$ratioh = $max_height/$height;
						$ratiow = $max_width/$width;
						$ratio = min($ratioh, $ratiow);
						$width = intval($ratio*$width);
						$height = intval($ratio*$height);
					} else {
						$img_path = "../images/no-icon.jpg";
						$width = 48;
						$height = 48;
					}
					echo '
                    <div class="base">
                      <ul class="base_row">
                        <li class="base_img">
                            <img src="'.$img_path.'" width="'.$width.'" height="'.$height.'" alt="" class="img_input">
                        </li>
                        <li class="base_name">
                            <h3><a href="">'.$row_equipment["name"].'</a></h3>
                        </li>
                        <li class="base_description">
                            <p>'.$row_equipment["description"].'</p>
                        </li>
                      </ul>
                    </div>
                ';
				}
				while ($row_equipment = mysqli_fetch_array($data_equipment));
			}
			?>

			<div class="admin_bottom_number">
                <?php
                for ($i = 1; $i <= 10; $i++) {
                    echo '
                <div class="number">
                    <h3>'.$i.' </h3>
                </div>
                ';
                }
                ?>
			</div>
		</div>
		<footer>
		<p>© 2007–2018 Артур Захаров, по всем вопросам пишите по адресу tibirlayn@gmail.com</p>
		</footer>

		<!-- Optional JavaScript -->
		<!-- jQuery first, then Popper.js, then Bootstrap JS -->
		<script type="text/javascript" src="/js/jquery-3.2.1.js"></script>
		<script type="text/javascript" src="/js/click.js"></script>
		<!--<script type="text/javascript" src="/js/bootstrap.js"></script>-->
		</body>
</html>


