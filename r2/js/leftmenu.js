$(document).ready(function() {
    var n = 205;
    menu_bar.onclick = function() {
        animate(function(timePassed) {
            menu_bar.style.left = timePassed / 1 + 'px';
            left_menu.style.left = timePassed / 1 + 'px';
        }, n);
    };

    // Рисует функция draw
    // Продолжительность анимации duration
    function animate(draw, duration) {
        if (menu_bar.style.left = n ){
            n = 0;
        } else
        {
            n = 205;
        }
        var start = performance.now();

        requestAnimationFrame(function animate(time) {
            // определить, сколько прошло времени с начала анимации
            var timePassed = time - start;

            console.log(time, start)
            // возможно небольшое превышение времени, в этом случае зафиксировать конец
            if (timePassed > duration) timePassed = duration;

            // нарисовать состояние анимации в момент timePassed
            draw(timePassed);

            // если время анимации не закончилось - запланировать ещё кадр
            if (timePassed < duration) {
                requestAnimationFrame(animate);
            }

        });
    }
});
