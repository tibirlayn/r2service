<?php
	include ("admin/connection_info.php");
	$link = mysqli_connect(con_localhost, con_user, con_password, con_db);
?>
<!DOCTYPE html>
<html lang="ru">
        <head>
			<!-- Required meta tags -->
			<meta charset="utf-8">
			<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

			<title>R2Servicedb</title>

			<!-- Bootstrap CSS -->
			<!--<link rel="stylesheet" type="text/css" href="css/bootstrap.css">﻿-->
			<link rel="shortcut icon" href="images/main_title.jpg" type="image/jpeg">
			<link rel="stylesheet" href="css/reset.css">
			<link rel="stylesheet" href="css/index.css">

		</head>
        <body>

		<div class="left_menu" id="left_menu" >
			<ul>
				<li><a href="">БАШНЯ МЕТЕОСА</a>
					<ul>
						<li><a href="">БАШНЯ МЕТЕОСА 1 УР (ЗЕМЛЯ)</a>
						<li><a href="">БАШНЯ МЕТЕОСА 2 УР (ВОЗДУХ)</a>
						<li><a href="">БАШНЯ МЕТЕОСА 3 УР (ОКЕАН)</a>
						<li><a href="">БАШНЯ МЕТЕОСА 4 УР (СОЛНЦЕ)</a>
					</ul>
				</li>
				<li><a href="">БАШНЯ ОГНЯ </a>
					<ul>
						<li><a href="">БАШНЯ ОГНЯ 1 УР</a></li>
						<li><a href="">БАШНЯ ОГНЯ 2 УР</a></li>
						<li><a href="">БАШНЯ ОГНЯ 3 УР</a></li>
						<li><a href="">БАШНЯ ОГНЯ 4 УР</a></li>
						<li><a href="">БАШНЯ ОГНЯ 5 УР</a></li>
					</ul>
				</li>
				<li><a href="">БЕЗЫМЯННАЯ ПЕЩЕРА </a>
					<ul>
						<li><a href="">БЕЗЫМЯННАЯ ПЕЩЕРА 1 УР</a></li>
						<li><a href="">БЕЗЫМЯННАЯ ПЕЩЕРА 2 УР</a></li>
					</ul>
				</li>
				<li><a href="">БОЛОТО ЧЕРНОГО ДРАКОНА</a>
					<ul>
						<li><a href="">БОЛОТО ЧЕРНОГО ДРАКОНА 1 УР</a></li>
						<li><a href="">БОЛОТО ЧЕРНОГО ДРАКОНА 2 УР</a></li>
						<li><a href="">БОЛОТО ЧЕРНОГО ДРАКОНА 3 УР</a></li>
						<li><a href="">БОЛОТО ЧЕРНОГО ДРАКОНА 4 УР</a></li>
						<li><a href="">БОЛОТО ЧЕРНОГО ДРАКОНА 5 УР</a></li>
					</ul>
				</li>
				<li><a href="">ГОРИЗОНТ ПОЛНОЙ ЛУНЫ </a></li>
				<li><a href="">ГРОБНИЦА КОРОЛЯ</a>
					<ul>
						<li><a href="">ГРОБНИЦА КОРОЛЯ 1 УР</a></li>
						<li><a href="">ГРОБНИЦА КОРОЛЯ 2 УР</a></li>
						<li><a href="">ГРОБНИЦА КОРОЛЯ 3 УР</a></li>
						<li><a href="">ГРОБНИЦА КОРОЛЯ 4 УР</a></li>
					</ul>
				</li>
				<li><a href="">ЗАЛ ИСПЫТАНИЙ</a></li>
				<li><a href="">КРЕПОСТЬ АБЕЛЛУСА</a></li>
				<li><a href="">ЛЕСТНИЦА ДРЕВНИХ</a>
					<ul>
						<li><a href="">ЛЕСТНИЦА ДРЕВНИХ	1 УР</a></li>
						<li><a href="">ЛЕСТНИЦА ДРЕВНИХ	2 УР</a></li>
						<li><a href="">ЛЕСТНИЦА ДРЕВНИХ 3 УР</a></li>
					</ul>
				</li>
				<li><a href="">ЛОГОВО ЖУКОВ</a>
					<ul>
						<li><a href="">ЛОГОВО ЖУКОВ 1 УР</a></li>
						<li><a href="">ЛОГОВО ЖУКОВ 2 УР</a></li>
						<li><a href="">ЛОГОВО ЖУКОВ 3 УР</a></li>
					</ul>
				</li>
				<li><a href="">НЕБЕСНЫЙ ОСТРОВ ЭЛЬТЕР</a></li>
				<li><a href="">ОЗЕРО ПОЛНОЙ ЛУНЫ</a></li>
				<li><a href="">ОСТРОВ АКРА</a></li>
				<li><a href="">ОСТРОВ ГВИНЕЯ</a></li>
				<li><a href="">ОСТРОВ КОЛФОРТ</a></li>
				<li><a href="">ПЕЩЕРА ДРЕВНИХ ПИРАТОВ</a>
					<ul>
						<li><a href="">ПЕЩЕРА ДРЕВНИХ ПИРАТОВ 1 УР</a></li>
						<li><a href="">ПЕЩЕРА ДРЕВНИХ ПИРАТОВ 2 УР</a></li>
					</ul>
				</li>
				<li><a href="">ПЕЩЕРА ЭГИРА</a>
					<ul>
						<li><a href="">ПЕЩЕРА ЭГИРА 1 УР</a></li>
						<li><a href="">ПЕЩЕРА ЭГИРА 2 УР</a></li>
						<li><a href="">ПЕЩЕРА ЭГИРА 3 УР</a></li>
					</ul>
				</li>
				<li><a href="">ПОДЗЕМЕЛЬЕ БААЛЬБЕКА</a>
					<ul>
						<li><a href="">ПОДЗЕМЕЛЬЕ БААЛЬБЕКА 1 УР</a></li>
						<li><a href="">ПОДЗЕМЕЛЬЕ БААЛЬБЕКА 2 УР</a></li>
					</ul>
				</li>
				<li><a href="">ПОДЗЕМЕЛЬЕ ВАЛЛЕФОРА</a>
					<ul>
						<li><a href="">ПОДЗЕМЕЛЬЕ ВАЛЛЕФОРА 1 УР</a></li>
						<li><a href="">ПОДЗЕМЕЛЬЕ ВАЛЛЕФОРА 2 УР</a></li>
						<li><a href="">ПОДЗЕМЕЛЬЕ ВАЛЛЕФОРА 3 УР</a></li>
					</ul>
				</li>
				<li><a href="">ПОДЗЕМЕЛЬЕ НЕЖИТИ</a>
					<ul>
						<li><a href="">ПОДЗЕМЕЛЬЕ НЕЖИТИ 1 УР</a></li>
						<li><a href="">ПОДЗЕМЕЛЬЕ НЕЖИТИ 2 УР</a></li>
						<li><a href="">ПОДЗЕМЕЛЬЕ НЕЖИТИ 3 УР</a></li>
						<li><a href="">ПОДЗЕМЕЛЬЕ НЕЖИТИ 4 УР</a></li>
					</ul>
				</li>
				<li><a href="">ПОДЗЕМЕЛЬЕ ЮПИТЕРА</a>
					<ul>
						<li><a href="">ПОДЗЕМЕЛЬЕ ЮПИТЕРА 1 УР</a></li>
						<li><a href="">ПОДЗЕМЕЛЬЕ ЮПИТЕРА 2 УР</a></li>
					</ul>
				</li>
				<li><a href="">ПРЕМИУМ ПОДЗЕМЕЛЬЕ</a>
					<ul>
						<li><a href="">ПРЕМИУМ ПОДЗЕМЕЛЬЕ 1 УР</a></li>
						<li><a href="">ПРЕМИУМ ПОДЗЕМЕЛЬЕ 2 УР</a></li>
					</ul>
				</li>
				<li><a href="">РУИНЫ ПОЛНОЛУНИЯ</a></li>
				<li><a href="">СВЯТЫЕ ЗЕМЛИ ИЛЛЮМИНА</a></li>
				<li><a href="">ТЕМНЫЕ ПЕЩЕРЫ</a>
					<ul>
						<li><a href="">ТЕМНЫЕ ПЕЩЕРЫ 1 УР</a></li>
						<li><a href="">ТЕМНЫЕ ПЕЩЕРЫ 2 УР</a></li>
						<li><a href="">ТЕМНЫЕ ПЕЩЕРЫ 3 УР</a></li>
						<li><a href="">ТЕМНЫЕ ПЕЩЕРЫ 4 УР</a></li>
						<li><a href="">ТЕМНЫЕ ПЕЩЕРЫ 5 УР</a></li>
					</ul>
				</li>
				<li><a href="">ХРАМ МЕТЕОСА</a>
					<ul>
						<li><a href="">ХРАМ МЕТЕОСА 1 УР</a></li>
						<li><a href="">ХРАМ МЕТЕОСА 2 УР</a></li>
					</ul>
				</li>
			</ul>
		</div>
		<div class="contenido">
			<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAABiSURBVGhD7dYxDYQAEADBq+neACJAyztABCKoEYEWtGABLtR0hIJjJlkPGwBw069ATRZ7geYstgJNGQAAD+oL1GaXW/y2zo1fCzRmAAA86F+gLrvc4rd1bvxSoCEDgK+LOACajShumfpRAQAAAABJRU5ErkJggg=="
				 id="menu_bar">
		</div>
		<!-- Optional JavaScript -->
		<!-- jQuery first, then Popper.js, then Bootstrap JS -->
		<script type="text/javascript" src="/js/jquery-3.2.1.js"></script>
		<script type="text/javascript" src="/js/click.js"></script>
		<script type="text/javascript" src="/js/leftmenu.js"></script>
		<!--<script type="text/javascript" src="/js/bootstrap.js"></script>-->
		</body>
</html>


