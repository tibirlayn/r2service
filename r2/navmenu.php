<?php
	include ("admin/connection_info.php");
	$link = mysqli_connect(con_localhost, con_user, con_password, con_db);
?>
<!DOCTYPE html>
<html lang="ru">
        <head>
			<!-- Required meta tags -->
			<meta charset="utf-8">
			<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

			<title>R2Servicedb</title>

			<!-- Bootstrap CSS -->
			<!--<link rel="stylesheet" type="text/css" href="css/bootstrap.css">﻿-->
			<link rel="shortcut icon" href="images/main_title.jpg" type="image/jpeg">
			<link rel="stylesheet" href="css/reset.css">
			<link rel="stylesheet" href="css/index.css">

		</head>
        <body>

		<nav>
			<a href="#" class="menu-trigger">Menu</a>
			<ul>
				<li><a href="#">Монстры</a></li>
				<li><a href="#">Материалы</a>
					<ul>
						<li><a href="">Производственные</a></li>
						<li><a href="">Свитки усиления</a></li>
						<li><a href="">Жезлы</a></li>
						<li><a href="">Книги</a></li>
						<li><a href="">Другое</a></li>
					</ul>
				</li>
				<li><a href="#">Обмундирование</a>
					<ul>
						<li><a href="">Оружие ближнего боя</a></li>
						<li><a href="">Оружие дальнего боя</a></li>
						<li><a href="">Доспехи</a></li>
						<li><a href="">Доп. защита</a></li>
						<li><a href="">Перчатки</a></li>
						<li><a href="">Сапоги</a></li>
						<li><a href="">Шлемы</a></li>
						<li><a href="">Плащи</a></li>
						<li><a href="">Аксессуары</a></li>
					</ul>
				</li>
				<li><a href="#">Сферы</a>
					<ul>
						<li><a href="">Сферы души</a></li>
						<li><a href="">Сферы жизни</a></li>
						<li><a href="">Сферы мастерства</a></li>
						<li><a href="">Сферы защиты</a></li>
						<li><a href="">Сферы разрушения</a></li>
						<li><a href="">Сферы характеристик</a></li>
					</ul>
				</li>
				<li><a href="#">Руны</a>
					<ul>
						<li><a href="">Руны оружия</a></li>
						<li><a href="">Руны доспехов</a></li>
						<li><a href="">Руны шлемов</a></li>
						<li><a href="">Руны перчаток</a></li>
						<li><a href="">Руны сапог</a></li>
						<li><a href="">Руны плащей</a></li>
						<li><a href="">Руны браслетов/щитов</a></li>
						<li><a href="">Временные руны</a></li>
						<li><a href="">Сундуки с рунами</a></li>
					</ul>
				</li>
				<li><a href="#">Другое</a>
					<ul>
						<li><a href="">Перевоплощения</a></li>
						<li><a href="">Питомец</a></li>
						<li><a href="">Квест</a></li>
					</ul>
				</li>
				<li><a href="#">Услуги</a>
					<ul>
						<li><a href="">Персонажи</a></li>
						<li><a href="">Гарант</a></li>
						<li><a href="">Серебро</a></li>
						<li><a href="">Кидалы</a></li>
                        <li><a href="">Инфо</a></li>
					</ul>
				</li>
			</ul>
		</nav>

		<!-- Optional JavaScript -->
		<!-- jQuery first, then Popper.js, then Bootstrap JS -->
		<script type="text/javascript" src="/js/jquery-3.2.1.js"></script>
		<script type="text/javascript" src="/js/click.js"></script>
		<!--<script type="text/javascript" src="/js/bootstrap.js"></script>-->
		</body>
</html>


