<?php
include ("black_list.php");
    include ("connection_info.php");
    $link = mysqli_connect(con_localhost, con_user, con_password, con_db);
?>

<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Руны</title>
    <link rel="shortcut icon" href="../images/admin.ico" type="image/x-icon">
    <link rel="stylesheet" href="../css/reset.css">
    <link rel="stylesheet" type="text/css" href="../css/style.css">
</head>
<body>
<div class="admin_conteiner">
<?php
    include ("main_top.php");
    include ("main.php");
?>
    <div class="admin_conteiner_main">
        <div class="admin_conteiner_main_h">
            <div class="admin_conteiner_mein_li">
                <H3>РУНЫ</H3>
                <li><a id="select_sort">Без сортировки</a>
                    <ul id="sorting_list">
                        <li><a href="">Руны оружия</a></li>
                        <li><a href="">Руны доспехов</a></li>
                        <li><a href="">Руны шлемов</a></li>
                        <li><a href="">Руны перчаток</a></li>
                        <li><a href="">Руны сапог</a></li>
                        <li><a href="">Руны плащей</a></li>
                        <li><a href="">Руны браслетов/щитов</a></li>
                        <li><a href="">Временные руны</a></li>
                        <li><a href="">Сундуки с рунами</a></li>
                    </ul>
                </li>
            </div>
            <a href="add_menu.php">Добавить</a>
        </div>

        <?php
        $query_equipment = "SELECT * FROM `object` WHERE visible = 1";
        $data_equipment = mysqli_query($link, $query_equipment);

        if (mysqli_num_rows($data_equipment) > 0){
            $row_equipment = mysqli_fetch_array($data_equipment);
            do{

                if ($row_equipment["image"] != "" && file_exists("../images/r2_icon/".$row_equipment["image"].".jpg")){

                    $img_path = '../images/r2_icon/'.$row_equipment["image"].".jpg";
                    $max_width = 48;
                    $max_height = 48;
                    list($width, $height) = getimagesize($img_path);
                    $ratioh = $max_height/$height;
                    $ratiow = $max_width/$width;
                    $ratio = min($ratioh, $ratiow);
                    $width = intval($ratio*$width);
                    $height = intval($ratio*$height);
                } else {
                    $img_path = "../images/no-icon.jpg";
                    $width = 48;
                    $height = 48;
                }
                echo '
                    <div class="base">
                      <ul class="base_row">
                        <li class="base_img">
                            <img src="'.$img_path.'" width="'.$width.'" height="'.$height.'" alt="" class="img_input">
                        </li>
                        <li class="base_name">
                            <h3><a href="">'.$row_equipment["name"].'</a></h3>
                        </li>
                        <li class="base_description">
                            <p>'.$row_equipment["description"].'</p>
                        </li>
                        <li class="base_dell_change">
                          <a class="input_dell" href="">Удалить</a>
                          <a class="input_change" href="">Изменить</a>
                        </li>
                      </ul>
                    </div>
                ';
            }
            while ($row_equipment = mysqli_fetch_array($data_equipment));
        }
        ?>



        <div class="admin_bottom_number">

            <h3> 1 </h3>

        </div>

    </div>
</div>
    <?php
    include ("black_list_end.php");
    ?>
    </body>
    </html>




