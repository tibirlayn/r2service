<?php
include ("admin/black_list.php");

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>R2Servicedb</title>
    <link rel="shortcut icon" href="images/main_title.jpg" type="image/jpeg">
    <link rel="stylesheet" href="css/reset.css">
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
<div class="main">
    <div class="top_menu">
        <div class="nav_top_menu">
            <ul id="sddm">
                <li><a href="#">Монстры</a></li>
                <li><a href="#"
                       onmouseover="mopen('m1')"
                       onmouseout="mclosetime()">Материалы</a>
                    <div id="m1"
                         onmouseover="mcancelclosetime()"
                         onmouseout="mclosetime()">
                        <a href="#">Камни</a>
                        <a href="#">Производственные</a>
                        <a href="#">Квестовые</a>
                        <a href="#">Еда</a>
                        <a href="#">Бутылки</a>
                        <a href="#">Настойки</a>
                        <a href="#">Свитки усиления</a>
                        <a href="#">Жезлы</a>
                        <a href="#">Книги</a>
                        <a href="#">Питомцы</a>
                        <a href="#">Трофеи</a>
                        <a href="#">Перевоплощения</a>
                        <a href="#">Другое</a>
                    </div>
                </li>
                <li><a href="#"
                       onmouseover="mopen('m2')"
                       onmouseout="mclosetime()">Обмундирование</a>
                    <div id="m2"
                         onmouseover="mcancelclosetime()"
                         onmouseout="mclosetime()">
                        <a href="#">Оружие ближнего боя</a>
                        <a href="#">Оружие дальнего боя</a>
                        <a href="#">Доспехи</a>
                        <a href="#">Доп. защита</a>
                        <a href="#">Перчатки</a>
                        <a href="#">Сапоги</a>
                        <a href="#">Шлемы</a>
                        <a href="#">Плащи</a>
                        <a href="#">Аксессуары</a>
                    </div>
                </li>
                <li><a href="#"
                       onmouseover="mopen('m3')"
                       onmouseout="mclosetime()">Сферы</a>
                    <div id="m3"
                         onmouseover="mcancelclosetime()"
                         onmouseout="mclosetime()">
                        <a href="#">Сферы души</a>
                        <a href="#">Сферы жизни</a>
                        <a href="#">Сферы мастерства</a>
                        <a href="#">Сферы защиты</a>
                        <a href="#">Сферы разрушения</a>
                        <a href="#">Сферы характеристик</a>
                    </div>
                </li>
                <li><a href="#"
                       onmouseover="mopen('m4')"
                       onmouseout="mclosetime()">Руны</a>
                    <div id="m4"
                         onmouseover="mcancelclosetime()"
                         onmouseout="mclosetime()">
                        <a href="#">Руны оружия</a>
                        <a href="#">Руны доспехов</a>
                        <a href="#">Руны шлемов</a>
                        <a href="#">Руны перчаток</a>
                        <a href="#">Руны сапог</a>
                        <a href="#">Руны плащей</a>
                        <a href="#">Руны браслетов/щитов</a>
                        <a href="#">Временные руны</a>
                        <a href="#">Сундуки с рунами</a>
                    </div>
                </li>
                <li><a href="#"
                       onmouseover="mopen('m5')"
                       onmouseout="mclosetime()">Другое</a>
                    <div id="m5"
                         onmouseover="mcancelclosetime()"
                         onmouseout="mclosetime()">
                        <a href="#">Квест</a>
                        <a href="#">Питомец</a>
                    </div>
                </li>
            </ul>
        </div>
    </div>
    <div class="mid_menu">
        <div class="block_search">
            <input type="text" pattern="[A-Za-z]+$"
                   maxlength="25" placeholder="Поиск"/>
        </div>
    </div>
    <div class="left_menu">
        <nav class="nav_menu">
            <ul>
                <li><a href="">БАШНЯ МЕТЕОСА</a>
                    <ul>
                        <li><a href="">БАШНЯ МЕТЕОСА 1 УР (ЗЕМЛЯ)</a>
                        <li><a href="">БАШНЯ МЕТЕОСА 2 УР (ВОЗДУХ)</a>
                        <li><a href="">БАШНЯ МЕТЕОСА 3 УР (ОКЕАН)</a>
                        <li><a href="">БАШНЯ МЕТЕОСА 4 УР (СОЛНЦЕ)</a>
                    </ul>
                </li>
                <li><a href="">БАШНЯ ОГНЯ </a>
                    <ul>
                        <li><a href="">БАШНЯ ОГНЯ 1 УР</a></li>
                        <li><a href="">БАШНЯ ОГНЯ 2 УР</a></li>
                        <li><a href="">БАШНЯ ОГНЯ 3 УР</a></li>
                        <li><a href="">БАШНЯ ОГНЯ 4 УР</a></li>
                        <li><a href="">БАШНЯ ОГНЯ 5 УР</a></li>
                    </ul>
                </li>
                <li><a href="">БЕЗЫМЯННАЯ ПЕЩЕРА </a>
                    <ul>
                        <li><a href="">БЕЗЫМЯННАЯ ПЕЩЕРА 1 УР</a></li>
                        <li><a href="">БЕЗЫМЯННАЯ ПЕЩЕРА 2 УР</a></li>
                    </ul>
                </li>
                <li><a href="">БОЛОТО ЧЕРНОГО ДРАКОНА</a>
                    <ul>
                        <li><a href="">БОЛОТО ЧЕРНОГО ДРАКОНА 1 УР</a></li>
                        <li><a href="">БОЛОТО ЧЕРНОГО ДРАКОНА 2 УР</a></li>
                        <li><a href="">БОЛОТО ЧЕРНОГО ДРАКОНА 3 УР</a></li>
                        <li><a href="">БОЛОТО ЧЕРНОГО ДРАКОНА 4 УР</a></li>
                        <li><a href="">БОЛОТО ЧЕРНОГО ДРАКОНА 5 УР</a></li>
                    </ul>
                </li>
                <li><a href="">ГОРИЗОНТ ПОЛНОЙ ЛУНЫ </a></li>
                <li><a href="">ГРОБНИЦА КОРОЛЯ</a>
                    <ul>
                        <li><a href="">ГРОБНИЦА КОРОЛЯ 1 УР</a></li>
                        <li><a href="">ГРОБНИЦА КОРОЛЯ 2 УР</a></li>
                        <li><a href="">ГРОБНИЦА КОРОЛЯ 3 УР</a></li>
                        <li><a href="">ГРОБНИЦА КОРОЛЯ 4 УР</a></li>
                    </ul>
                </li>
                <li><a href="">ЗАЛ ИСПЫТАНИЙ</a></li>
                <li><a href="">КРЕПОСТЬ АБЕЛЛУСА</a></li>
                <li><a href="">ЛЕСТНИЦА ДРЕВНИХ</a>
                    <ul>
                        <li><a href="">ЛЕСТНИЦА ДРЕВНИХ	1 УР</a></li>
                        <li><a href="">ЛЕСТНИЦА ДРЕВНИХ	2 УР</a></li>
                        <li><a href="">ЛЕСТНИЦА ДРЕВНИХ 3 УР</a></li>
                    </ul>
                </li>
                <li><a href="">ЛОГОВО ЖУКОВ</a>
                    <ul>
                        <li><a href="">ЛОГОВО ЖУКОВ 1 УР</a></li>
                        <li><a href="">ЛОГОВО ЖУКОВ 2 УР</a></li>
                        <li><a href="">ЛОГОВО ЖУКОВ 3 УР</a></li>
                    </ul>
                </li>
                <li><a href="">НЕБЕСНЫЙ ОСТРОВ ЭЛЬТЕР</a></li>
                <li><a href="">ОЗЕРО ПОЛНОЙ ЛУНЫ</a></li>
                <li><a href="">ОСТРОВ АКРА</a></li>
                <li><a href="">ОСТРОВ ГВИНЕЯ</a></li>
                <li><a href="">ОСТРОВ КОЛФОРТ</a></li>
                <li><a href="">ПЕЩЕРА ДРЕВНИХ ПИРАТОВ</a>
                    <ul>
                        <li><a href="">ПЕЩЕРА ДРЕВНИХ ПИРАТОВ 1 УР</a></li>
                        <li><a href="">ПЕЩЕРА ДРЕВНИХ ПИРАТОВ 2 УР</a></li>
                    </ul>
                </li>
                <li><a href="">ПЕЩЕРА ЭГИРА</a>
                    <ul>
                        <li><a href="">ПЕЩЕРА ЭГИРА 1 УР</a></li>
                        <li><a href="">ПЕЩЕРА ЭГИРА 2 УР</a></li>
                        <li><a href="">ПЕЩЕРА ЭГИРА 3 УР</a></li>
                    </ul>
                </li>
                <li><a href="">ПОДЗЕМЕЛЬЕ БААЛЬБЕКА</a>
                    <ul>
                        <li><a href="">ПОДЗЕМЕЛЬЕ БААЛЬБЕКА 1 УР</a></li>
                        <li><a href="">ПОДЗЕМЕЛЬЕ БААЛЬБЕКА 2 УР</a></li>
                    </ul>
                </li>
                <li><a href="">ПОДЗЕМЕЛЬЕ ВАЛЛЕФОРА</a>
                    <ul>
                        <li><a href="">ПОДЗЕМЕЛЬЕ ВАЛЛЕФОРА 1 УР</a></li>
                        <li><a href="">ПОДЗЕМЕЛЬЕ ВАЛЛЕФОРА 2 УР</a></li>
                        <li><a href="">ПОДЗЕМЕЛЬЕ ВАЛЛЕФОРА 3 УР</a></li>
                    </ul>
                </li>
                <li><a href="">ПОДЗЕМЕЛЬЕ НЕЖИТИ</a>
                    <ul>
                        <li><a href="">ПОДЗЕМЕЛЬЕ НЕЖИТИ 1 УР</a></li>
                        <li><a href="">ПОДЗЕМЕЛЬЕ НЕЖИТИ 2 УР</a></li>
                        <li><a href="">ПОДЗЕМЕЛЬЕ НЕЖИТИ 3 УР</a></li>
                        <li><a href="">ПОДЗЕМЕЛЬЕ НЕЖИТИ 4 УР</a></li>
                    </ul>
                </li>
                <li><a href="">ПОДЗЕМЕЛЬЕ ЮПИТЕРА</a>
                    <ul>
                        <li><a href="">ПОДЗЕМЕЛЬЕ ЮПИТЕРА 1 УР</a></li>
                        <li><a href="">ПОДЗЕМЕЛЬЕ ЮПИТЕРА 2 УР</a></li>
                    </ul>
                </li>
                <li><a href="">ПРЕМИУМ ПОДЗЕМЕЛЬЕ</a>
                    <ul>
                        <li><a href="">ПРЕМИУМ ПОДЗЕМЕЛЬЕ 1 УР</a></li>
                        <li><a href="">ПРЕМИУМ ПОДЗЕМЕЛЬЕ 2 УР</a></li>
                    </ul>
                </li>
                <li><a href="">РУИНЫ ПОЛНОЛУНИЯ</a></li>
                <li><a href="">СВЯТЫЕ ЗЕМЛИ ИЛЛЮМИНА</a></li>
                <li><a href="">ТЕМНЫЕ ПЕЩЕРЫ</a>
                    <ul>
                        <li><a href="">ТЕМНЫЕ ПЕЩЕРЫ 1 УР</a></li>
                        <li><a href="">ТЕМНЫЕ ПЕЩЕРЫ 2 УР</a></li>
                        <li><a href="">ТЕМНЫЕ ПЕЩЕРЫ 3 УР</a></li>
                        <li><a href="">ТЕМНЫЕ ПЕЩЕРЫ 4 УР</a></li>
                        <li><a href="">ТЕМНЫЕ ПЕЩЕРЫ 5 УР</a></li>
                    </ul>
                </li>
                <li><a href="">ХРАМ МЕТЕОСА</a>
                    <ul>
                        <li><a href="">ХРАМ МЕТЕОСА 1 УР</a></li>
                        <li><a href="">ХРАМ МЕТЕОСА 2 УР</a></li>
                    </ul>
                </li>
            </ul>
        </nav>
    </div>
    <div class="right_menu">
        <table class="table_menu">
            <tbody>
            <tr>
                <th>Вид</th>
                <th>Имя</th>
                <th>Уровень</th>
                <th>Подземелье</th>
                <th>Предметы</th>
            </tr>
            <tr>
                <td>photo</td>
                <td>Голая улитка</td>
                <td>A</td>
                <td>ТЕМНЫЕ ПЕЩЕРЫ 5 УР</td>
                <td>1 2 3 4 5 6 7 </td>
            </tr>
            <tr>
                <td>photo</td>
                <td>Голая улитка</td>
                <td>A</td>
                <td>БАШНЯ МЕТЕОСА 4 УР (СОЛНЦЕ)</td>
                <td>1 2 3 4 5 6 7 </td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
<script src="js/menu.js"></script>
<?php
include ("admin/black_list_end.php");
?>
</body>
</html>


