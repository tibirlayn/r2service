<?php
    include ("black_list.php");
    include ("connection_info.php");
    $link = mysqli_connect(con_localhost, con_user, con_password, con_db);
    ?>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Главное меню</title>
    <link rel="shortcut icon" href="../images/admin.ico" type="image/x-icon">
    <link rel="stylesheet" href="../css/reset.css">
    <link rel="stylesheet" type="text/css" href="../css/style.css">
    <script src="https://cdn.ckeditor.com/4.8.0/basic/ckeditor.js"></script>
    <script src="../js/click.js"></script>
</head>
<body>
<div class="admin_conteiner">
<?php
    include ("main_top.php");
    include ("main.php");
?>
    <div class="admin_conteiner_main">
        <div class="admin_conteiner_main_h">
            <h3>ДОБАВИТЬ</h3>
        </div>
        <div class="admin_conteiner_input_add">
            <from enctype="multipart/form-data" method="post">
                <input class="" name="name" type="text" placeholder="Название предмета" ><br>
                <h3 class="h3click">Описание предмета</h3>
                <div class="div-editor1">
                    <textarea name="txt1" id="editor1" cols="120" rows="20"></textarea>
                    <script type="text/javascript">
                        var ckeditor1 = CKEDITOR.replace("editor1");
                        AjexFileManager.init({
                            returnTo: "ckeditor",
                            editor: ckeditor1
                        });
                    </script>
                </div>
                <h2>ЭКИПИРОВКА</h2>
                <select>
                    <option>Оружие ближнего боя</option>
                    <option>Оружие дальнего боя</option>
                    <option>Доспехи</option>
                    <option>Доп. защита</option>
                    <option>Перчатки</option>
                    <option>Сапоги</option>
                    <option>Шлемы</option>
                    <option>Плащи</option>
                    <option>Аксессуары</option>
                </select>
                <h2>МАТЕРИАЛЫ</h2>
                <select>
                    <option>Камни</option>
                    <option>Производственные</option>
                    <option>Квестовые</option>
                    <option>Еда</option>
                    <option>Бутылки</option>
                    <option>Настойки</option>
                    <option>Свитки усиления</option>
                    <option>Жезлы</option>
                    <option>Книги</option>
                    <option>Питомцы</option>
                    <option>Трофеи</option>
                    <option>Перевоплощения</option>
                    <option>Другое</option>
                </select>
                <h2>СФЕРЫ</h2>
                <select>
                    <option>Сферы души</option>
                    <option>Сферы жизни</option>
                    <option>Сферы мастерства</option>
                    <option>Сферы защиты</option>
                    <option>Сферы разрушения</option>
                    <option>Сферы характеристик</option>
                </select>
                <h2>РУНЫ</h2>
                <select>
                    <option>Руны оружия</option>
                    <option>Руны доспехов</option>
                    <option>Руны шлемов</option>
                    <option>Руны перчаток</option>
                    <option>Руны сапог</option>
                    <option>Руны плащей</option>
                    <option>Руны браслетов/щитов</option>
                    <option>Временные руны</option>
                    <option>Сундуки с рунами</option>
                </select>
                <h2>ДРУГОЕ</h2>
                <select>
                    <option></option>
                    <option></option>
                    <option></option>
                    <option></option>
                    <option></option>
                    <option></option>
                    <option></option>
                    <option></option>
                    <option></option>
                </select>
                <h2>МОНСТЕРЫ</h2>
                <select>
                    <option>Монстер</option>
                    <option>БОСС</option>
                </select>

                <input class="admin_submit" name="submit" type="submit" value="Добавить фото">
                <input class="admin_submit" name="submit" type="submit" value="Изменить экипировку">
                <input class="admin_submit"  name="submit" type="submit" value="Добавить экипировку" >
            </from>
        </div>
    </div>
</div>
<script type="text/javascript" src="../ckeditor/ckeditor.js"></script>
    <?php
    include ("black_list_end.php");
    ?>
    </body>
    </html>




