<?php
    top('Восстановить пароль');
?>

<script src="/js/admin_login.js"></script>

<div class="row">

    <div class="col s12 m12 l6 xl5">
        <div class="container">
            <div class="row " id="top-height">
                <ul class="tabs ">
                    <li class="tab col s6 "><a class="active light-blue-text text-lighten-2" href="#test-swipe-1">Восстановить пароль</a></li>
                </ul>

                <div id="test-swipe-1" class="col s12">
                    <form class="col s12" id="form">
                        <div class="row">
                            <div class="input-field row s6">
                                <i class="material-icons prefix">person</i>
                                <input id="icon_name" name="email" type="text" class="validate">
                                <label id="icon_name_error" for="icon_name">Email</label>
                                <span class="helper-text"><label id="icon_name-error" class="error" for="icon_name"></label></span>
                            </div>
                        </div>
                        <div>
                            <div class="col s6 right-align">
                                <button class="btn waves-effect waves-light btn-large" type="submit" name="entry_submit">Восстановить
                                    <i class="material-icons right">send</i>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>

</div>

<script type="text/javascript" src="/js/jquery.validate.min.js"></script>
<script>
    $(document).ready(function(){
        $('.tabs').tabs({
        });

        $(".tabs>.indicator").css("background-color", '#4fc3f7');

        $("#form").validate({
            rules: {
                email: {
                        required: true,
                        email: true
                }
            },
            messages: {
                email: {
                        required: "Поле 'Email' обязательно к заполнению",
                        email: "Необходим формат адреса Email"
                }
            }
        });
        $("form").submit(function(){
            if($("#form").valid()){
                post_query('aulogin', 'recovery', 'email');
            }
        });
    });

</script>

<?php bot();?>
