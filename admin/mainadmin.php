<?php top('Главный');?>

        <nav>
                <div class="nav-wrapper light-blue lighten-2">
                    <div class="container">
                        <div class="left">
                            <a href="http://tilann.ru/" class="brand-logo center">Tilann</a>
                            <ul id="nav-mobile" class="right hide-on-med-and-down">
                            </ul>
                        </div>
                    </div>
            </div>
        </nav>
        
        <!--main-->
        <div class="container">
            <div class="row">
        <?php
                //include ("connection_info.php");
                //$link = mysqli_connect(con_localhost, con_user, con_password, con_db);

                $query_equipment = "SELECT * FROM `game`";
                $data_equipment = mysqli_query($link, $query_equipment);

                if (mysqli_num_rows($data_equipment) > 0){
                    $row_equipment = mysqli_fetch_array($data_equipment);
                    do{

                        if ($row_equipment["image"] != "" && file_exists("images/title/".$row_equipment["image"].".jpg")){
                            $img_path = 'images/title/'.$row_equipment["image"].".jpg";
                            $link_game = $row_equipment["link_admin"];
                        } else {
                            $img_path = "images/title/no-image.jpg";
                        }
                        echo '
                                                        <div class="col s12 m6 l6 xl4">
                                                            <div class="card hoverable">
                                                                <div class="card-image">
                                                                    <a href="'.$link_game.'">
                                                                        <img class="img_title" src="'.$img_path.'">
                                                                    </a>
                                                                </div>
                                                                <div class="card-content center-align">
                                                                    <p>'.$row_equipment["name"].'</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    ';
                    }
                    while ($row_equipment = mysqli_fetch_array($data_equipment));
                }

                //mysqli_query($link, $query) or die("Ошибка " . mysqli_error($link));
                mysqli_close($link);

         ?>
                 </div>
                </div>
                
                <script type="text/javascript" src="../js/jquery-3.2.1.js"></script>
                <script type="text/javascript" src="../js/materialize.min.js"></script>

<?php bot(); ?>
