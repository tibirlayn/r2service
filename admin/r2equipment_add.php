<?php top_admin('Добавить экипировку');?>

<div class="col">
    <div class="container">
        <div class="row">
            <form id="form">
                <div class="col s12 m12 l6 xl6">

                    <div class="input-field col s12">
                        <input name="name" id="name" type="text" class="validate">
                        <label for="name">Название</label>
                        <span class="helper-text"><label id="name-error" class="error" for="icon_name"></label></span>
                    </div>

                    <div class="input-field col s12">
                        <textarea name="description" id="description" class="materialize-textarea" data-length="500"></textarea>
                        <label for="description">Описание</label>
                        <span class="helper-text"><label id="description-error" class="error" for="icon_name"></label></span>
                    </div>

                    <div class="input-field col s12">
                        <select name="class_id[]" id="class_id_select" multiple>
                            <option value="0" selected>Персонаж (Все)</option>
                            <option value="1">Рыцарь</option>
                            <option value="2">Рейнджер</option>
                            <option value="3">Маг</option>
                            <option value="4">Призыватель</option>
                            <option value="5">Ассасин</option>
                        </select>
                    </div>

                    <div class="input-field col s12">
                        <select name="materials">
                            <option value="0" selected>Материалы</option>
                            <option value="1">Производственные</option>
                            <option value="2">Свитки усиления</option>
                            <option value="3">Жезлы</option>
                            <option value="4">Книги</option>
                            <option value="5">Другое</option>
                        </select>
                    </div>

                    <div class="input-field col s12">
                        <select name="outfit">
                            <option value="0" selected>Обмундирование</option>
                            <option value="1">Оружие ближнего боя</option>
                            <option value="2">Оружие дальнего боя</option>
                            <option value="3">Доспехи</option>
                            <option value="4">Доп. защита</option>
                            <option value="5">Перчатки</option>
                            <option value="6">Сапоги</option>
                            <option value="7">Шлемы</option>
                            <option value="8">Плащи</option>
                            <option value="9">Аксессуары</option>
                        </select>
                    </div>

                    <div class="input-field col s12">
                        <select name="spheres">
                            <option value="0" selected>Сферы</option>
                            <option value="1">Сферы души</option>
                            <option value="2">Сферы жизни</option>
                            <option value="3">Сферы мастерства</option>
                            <option value="4">Сферы защиты</option>
                            <option value="5">Сферы разрушения</option>
                            <option value="6">Сферы характеристик</option>
                        </select>
                    </div>

                    <div class="input-field col s12">
                        <select name="runes">
                            <option value="0" selected>Руны</option>
                            <option value="1">Руны оружия</option>
                            <option value="2">Руны доспехов</option>
                            <option value="3">Руны шлемов</option>
                            <option value="4">Руны перчаток</option>
                            <option value="5">Руны сапог</option>
                            <option value="6">Руны плащей</option>
                            <option value="7">Руны браслетов/щитов</option>
                            <option value="8">Временные руны</option>
                            <option value="9">Сундуки с рунами</option>
                        </select>
                    </div>

                    <div class="input-field col s12">
                        <select name="other">
                            <option value="0" selected>Другое</option>
                            <option value="1">Перевоплощения</option>
                            <option value="2">Питомец</option>
                            <option value="3">Квест</option>
                        </select>
                    </div>

                    <!--
                     <div class="col s12 m12 l12 xl12">
                        <div class="file-field input-field">
                            <div class="btn">
                                <span>File</span>
                                <input type="file" name="image" multiple>
                            </div>
                            <div class="file-path-wrapper">
                                <input name="file_image" class="file-path validate" type="text" placeholder="Upload one or more files">
                            </div>
                        </div>
                    </div>
                    -->

                    <div class="center-align">
                        <button class="btn waves-effect waves-light btn-large pulse" type="submit" name="send">ОТПРАВИТЬ
                            <i class="material-icons right">send</i>
                        </button>
                    </div>
                </div>

            </form>
        </div>

    </div>
</div>


<script type="text/javascript" src="../js/side-nav.js"></script>
<script type="text/javascript" src="../js/jquery.formatter.min.js"></script>
<script type="text/javascript" src="../js/jquery.validate.min.js"></script>
<script>
    $(document).ready(function () {
        $('.dropdown-trigger').dropdown();
        $('select').formSelect();
        $('textarea#description').characterCounter();

        $("form").submit(function(){
            if($("#form").valid()){
                var class_id = $("#class_id_select").val();
                var arr_class;
                $.each(class_id, function(key,value){
                    arr_class = [class_id[0], class_id[1], class_id[2], class_id[3], class_id[4], class_id[5]];
                });

                //name, description, materials, outfit, spheres, runes, other
                //name : name, description : description, materials : materials, outfit : outfit, spheres : spheres, runes : runes, other : other
                //{class_id : class_id }
                $.ajax({
                    type: "POST",
                    url: "r2equipment_add_file.php",
                    data: $(this).serialize()
                }).done(function(data) {
                    console.log(data);
                    // var response = JSON.parse(data);
                    // console.log(response);
                    M.toast({html: 'Заявка отправлена!'});
                });
                return false;
            }
        });

    });
</script>


<?php bot(); ?>