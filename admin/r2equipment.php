<?php

top_admin('Экипировка');
?>

<div class="col">

        <div class="container">
            <table>
                <thead>
                <tr>
                    <th style="width: 150px;">Вид</th>
                    <th style="width: 200px;">Название</th>
                    <th>Описание</th>
                    <th>Класс</th>
                    <th style="width: 150px;">Свойства</th>
                </tr>
                </thead>

                <?php
                $del = $_GET['del'];
                if (isset($del)){
                    $delete_element = "DELETE FROM `object` WHERE object_id = '$del'";
                    $delete = mysqli_query($link, $delete_element);
                }

                $query_equipment = "SELECT * FROM `object` WHERE visible = 1";
                $data_equipment = mysqli_query($link, $query_equipment);

                if (mysqli_num_rows($data_equipment) > 0){
                    $row_equipment = mysqli_fetch_array($data_equipment);
                    do{

                        if ($row_equipment["image"] != "" && file_exists("images/r2/".$row_equipment["image"].".jpg")){

                            $img_path = 'images/r2/'.$row_equipment["image"].".jpg";
                            $max_width = 48;
                            $max_height = 48;
                            list($width, $height) = getimagesize($img_path);
                            $ratioh = $max_height/$height;
                            $ratiow = $max_width/$width;
                            $ratio = min($ratioh, $ratiow);
                            $width = intval($ratio*$width);
                            $height = intval($ratio*$height);
                        } else {
                            $img_path = "../images/r2/no-icon.jpg";
                            $width = 48;
                            $height = 48;
                        }
                        echo '
                            <tbody>
                            <tr>
                                <td><img src="'.$img_path.'" width="'.$width.'" height="'.$height.'" alt="" class="img_input"></td>
                                <td>'.$row_equipment["name"].'</td>
                                <td>'.$row_equipment["description"].'</td>
                                <td>'.$row_equipment["class_image"].'</td>
                                <td>
                                <ul>
                                    <li><a href="#del'.$row_equipment["object_id"].'" class="waves-effect waves-light btn-floating btn-large pulse modal-trigger"><i class="small material-icons">delete_forever</i></a></li>
                                    <li><a href="#edit'.$row_equipment["object_id"].'" class="waves-effect waves-light btn-floating btn-large cyan pulse modal-trigger"><i class="small material-icons">edit</i></a></li>
                                </ul>    
                                </td>
                            </tr>
                            </tbody>
                            
                            <!----------------- Delete --------------------->
                            <div id="del'.$row_equipment["object_id"].'" class="modal">
                                <div class="modal-content">
                                    <h4>Удалить запись?</h4>
                                    <p>Вы дейтсвительно хотите удалить данную запись?</p>
                                </div>
                                <div class="modal-footer">
                                    <a class="modal-close waves-effect waves-green btn-flat">Disagree</a>
                                    <a href="?del='.$row_equipment["object_id"].'" class="modal-close waves-effect waves-green btn-flat">Agree</a>
                                </div>
                            </div>
                            <!----------------- Add ----------------------->
                            <div id="edit'.$row_equipment["object_id"].'" class="modal">
                                <div class="modal-content">
                                    <h4>Редактировать запись?</h4>
                                    <p>Вы дейтсвительно хотите редоктировать данную запись?</p>
                                </div>
                                <div class="modal-footer">
                                    <a class="modal-close waves-effect waves-green btn-flat">Disagree</a>
                                    <a href="?edit='.$row_equipment["object_id"].'" class="modal-close waves-effect waves-green btn-flat">Agree</a>
                                </div>
                            </div>
                ';
                    }
                    while ($row_equipment = mysqli_fetch_array($data_equipment));
                }
                ?>
        </table>
    </div>
</div>

<!--JavaScript at end of body for optimized loading-->
<script type="text/javascript" src="../js/jquery-3.2.1.js"></script>
<script type="text/javascript" src="../js/materialize.min.js"></script>
<script type="text/javascript" src="../js/side-nav.js"></script>
<script>
    $(document).ready(function () {
        $('.dropdown-trigger').dropdown();

        document.addEventListener('DOMContentLoaded', function() {
        var elems = document.querySelectorAll('.modal');
        var instances = M.Modal.init(elems, options);
    });

    // Or with jQuery

    $(document).ready(function(){
        $('.modal').modal();
    });
    });
</script>


<?php bot(); ?>
