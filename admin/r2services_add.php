<?php top_admin('R2 services add');

$id = $_GET['id'];
if (isset($id)){
            $delete_element = "DELETE FROM `garant` WHERE garant_id = '$id'";
            $delete = mysqli_query($link, $delete_element);
}
$add = $_GET['add'];
if (isset($add)){
    $add_element = "UPDATE `garant` SET visible = 1 WHERE garant_id = '$add'";
    $add = mysqli_query($link, $add_element);
}
?>

<div class="col">
        <div class="container">
            <div class="row">

                <?php
                $query_equipment = "SELECT * FROM `garant` WHERE visible = 0";
                $data_equipment = mysqli_query($link,$query_equipment);
                if (mysqli_num_rows($data_equipment) > 0) {
                $row_equipment = mysqli_fetch_array($data_equipment);
                do {
                echo '
                <div class="col s12 m6 l4 xl4">
                    <div class="card hoverable">
                        <div class="card light-blue lighten-2">
                            <div class="card-content white-text">
                                <span class="card-title center-align">'.$row_equipment["buy_sell"].'</span>
                            </div>
                        </div>
                        <div class="card-image center-align">
                            <table>
                                <tbody>
                                <tr>
                                    <td>Сервер</td>
                                    <td>'.$row_equipment["server"].'</td>
                                </tr>
                                <tr>
                                    <td>Класс</td>
                                    <td>'.$row_equipment["class"].'</td>
                                </tr>
                                <tr>
                                    <td>Уровень</td>
                                    <td>'.$row_equipment["level"].'</td>
                                </tr>
                                <tr>
                                    <td>Питомец</td>
                                    <td>'.$row_equipment["pet"].'</td>
                                </tr>
                                <tr>
                                    <td>Ур. питомца</td>
                                    <td>'.$row_equipment["level_pet"].'</td>
                                </tr>
                                <tr>
                                    <td>Камень питомца</td>
                                    <td>'.$row_equipment["stone_pet"].'</td>
                                </tr>
                                <tr>
                                    <td>Питомец грейд</td>
                                    <td>'.$row_equipment["pet_grade"].'</td>
                                </tr>
                                <tr>
                                    <td>Маска</td>
                                    <td>'.$row_equipment["mask"].'</td>
                                </tr>
                                <tr>
                                    <td>Золотая корона</td>
                                    <td>'.$row_equipment["gold_crown"].'</td>
                                </tr>
                                <tr>
                                    <td>Ф.И.О.</td>
                                    <td>'.$row_equipment["fio"].'</td>
                                </tr>
                                <tr>
                                    <td>Паспорт</td>
                                    <td>'.$row_equipment["pasport"].'</td>
                                </tr>
                                <tr>
                                    <td>КОСы</td>
                                    <td>'.$row_equipment["kos"].'</td>
                                </tr>
                                <tr>
                                    <td>Цена</td>
                                    <td>'.$row_equipment["price"].' '.$row_equipment["course"].'</td>
                                </tr>
                                <tr>
                                    <td>Связь</td>
                                    <td><a href="'.$row_equipment["contact"].'">'.$row_equipment["name_user"].'</a></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="row center-align">
                            <a href="#visible'.$row_equipment["garant_id"].'" class="waves-effect waves-light btn-floating btn-large pulse orange darken-2 modal-trigger"><i class="material-icons">playlist_add</i></a>
                            <a href="#'.$row_equipment["garant_id"].'" class="waves-effect waves-light btn-floating btn-large pulse red lighten-1 modal-trigger"><i class="material-icons">delete_sweep</i></a>
                        </div>
                        <!----------------- Delete --------------------->
                        <div id="'.$row_equipment["garant_id"].'" class="modal">
                            <div class="modal-content">
                                <h4>Удалить запись?</h4>
                                <p>Вы дейтсвительно хотите удалить данную запись?</p>
                            </div>
                            <div class="modal-footer">
                                <a class="modal-close waves-effect waves-green btn-flat">Disagree</a>
                                <a href="?id='.$row_equipment["garant_id"].'" class="modal-close waves-effect waves-green btn-flat">Agree</a>
                            </div>
                        </div>
                        <!----------------- Add ----------------------->
                        <div id="visible'.$row_equipment["garant_id"].'" class="modal">
                            <div class="modal-content">
                                <h4>Добавить запись?</h4>
                                <p>Вы дейтсвительно хотите добавить данную запись?</p>
                            </div>
                            <div class="modal-footer">
                                <a class="modal-close waves-effect waves-green btn-flat">Disagree</a>
                                <a href="?add='.$row_equipment["garant_id"].'" class="modal-close waves-effect waves-green btn-flat">Agree</a>
                            </div>
                        </div>
                    </div>
                </div>
                       ';
                    } while ($row_equipment = mysqli_fetch_array($data_equipment));
                }
                ?>
            </div>
        </div>
</div>

<script type="text/javascript" src="/js/nouislider.js"></script>
<script type="text/javascript" src="/js/range.js"></script>
<script>
    $(document).ready(function () {
        document.addEventListener('DOMContentLoaded', function() {
            var elems = document.querySelectorAll('.modal');
            var instances = M.Modal.init(elems, options);
        });

        // Or with jQuery

        $(document).ready(function(){
            $('.modal').modal();
        });

    });
</script>


<?php bot();?>
