<?php top_admin('R2 admin');?>

    <script src="/js/admin_login.js"></script>

    <div class="row">
        <div class="container">
            <div class="row" id="top-height">
                <ul class="tabs ">
                    <li class="tab col s6 "><a class="active light-blue-text text-lighten-2" href="#test-swipe-1">Регистрация</a></li>
                    <li class="tab col s6"><a class="light-blue-text text-lighten-2" href="#test-swipe-2">Пользователи</a></li>
                </ul>
                <!-- Регистрация -->
                <div id="test-swipe-1" class="col s12">
                    <form class="col s12" id="form">
                        <div class="row">
                            <div class="input-field row s6">
                                <i class="material-icons prefix">account_circle</i>
                                <input id="icon_login" name="login" type="text" class="validate">
                                <label id="icon_login_error" for="icon_login">Login</label>
                                <span class="helper-text"><label id="icon_login-error" class="error" for="icon_login"></label></span>
                            </div>
                            <div class="input-field row s6">
                                <i class="material-icons prefix">markunread</i>
                                <input id="icon_email" name="email" type="email"  class="validate">
                                <label id="icon_email_error" for="icon_email">Email</label>
                                <span class="helper-text"><label id="icon_email-error" class="error" for="icon_email"></label></span>
                            </div>
                            <div class="input-field row s6">
                                <i class="material-icons prefix">lock</i>
                                <input id="icon_password" name="password" type="password"  class="validate">
                                <label id="icon_password_error" for="icon_password">Password</label>
                                <span class="helper-text"><label id="icon_password_error" class="error" for="icon_password"></label></span>
                            </div>
                            <div class="input-field row s6">
                                <i class="material-icons prefix">lock_outline</i>
                                <input id="icon_r_password" name="r_password" type="password" class="validate">
                                <label id="icon_r_password_error" for="icon_r_password">Repeat password</label>
                                <span class="helper-text"><label id="icon_r_password-error" class="error" for="icon_r_password"></label></span>
                            </div>
                        </div>
                        <div class="right-align">
                            <button class="btn waves-effect waves-light btn-large" id="submit" type="submit" name="reg_submit">Регистрация
                                <i class="material-icons right">send</i>
                            </button>
                        </div>
                    </form>
                </div>
            </div>

            <!-- Пользователи -->
            <div id="test-swipe-2" class="col s12">
                <table>
                    <thead>
                    <tr>
                        <th>Login</th>
                        <th>email</th>
                        <th>Свойства</th>
                    </tr>
                    </thead>
                    <?php

                    $del_user = $_GET['del_user'];
                    if (isset($del_user)){
                        $delete_element = "DELETE FROM `users` WHERE user_id = '$del_user'";
                        $delete = mysqli_query($link, $delete_element);
                    }

                    $query_equipment = "SELECT * FROM `users`";
                    $data_equipment = mysqli_query($link, $query_equipment);

                    if (mysqli_num_rows($data_equipment) > 0){
                        $row_equipment = mysqli_fetch_array($data_equipment);
                        do{
                            echo '
                            <tbody>
                            <tr>
                                <td>'.$row_equipment["login"].'</td>
                                <td>'.$row_equipment["email"].'</td>
                                <td>
                                <ul>
                                <a href="#edit'.$row_equipment["user_id"].'" class="waves-effect waves-light btn-floating btn-large pulse modal-trigger"><i class="small material-icons">edit</i></a>
                                <a href="#del_user'.$row_equipment["user_id"].'" class="waves-effect waves-light btn-floating btn-large cyan pulse modal-trigger"><i class="small material-icons">delete_forever</i></a>
                                </ul>    
                                </td>
                            </tr>
                            </tbody>
                            <!----------------- Delete --------------------->
                        <div id="edit'.$row_equipment["user_id"].'" class="modal">
                            <div class="modal-content">
                                <h4>Удалить запись?</h4>
                                <p>Вы дейтсвительно хотите удалить данную запись?</p>
                            </div>
                            <div class="modal-footer">
                                <a class="modal-close waves-effect waves-green btn-flat">Disagree</a>
                                <a href="?edit='.$row_equipment["user_id"].'" class="modal-close waves-effect waves-green btn-flat">Agree</a>
                            </div>
                        </div>
                        <!----------------- Add ----------------------->
                        <div id="del_user'.$row_equipment["user_id"].'" class="modal">
                            <div class="modal-content">
                                <h4>Редактировать запись?</h4>
                                <p>Вы дейтсвительно хотите редактировать данную запись?</p>
                            </div>
                            <div class="modal-footer">
                                <a class="modal-close waves-effect waves-green btn-flat">Disagree</a>
                                <a href="?del_user='.$row_equipment["user_id"].'" class="modal-close waves-effect waves-green btn-flat">Agree</a>
                            </div>
                        </div>
                ';
                        }
                        while ($row_equipment = mysqli_fetch_array($data_equipment));
                    }
                    ?>
                </table>
            </div>
        </div>



    </div>

    <!--JavaScript at end of body for optimized loading-->
    <script type="text/javascript" src="../js/jquery.validate.js"></script>
    <script>
        $(document).ready(function(){
            $('.tabs').tabs({
            });

            document.addEventListener('DOMContentLoaded', function() {
                var elems = document.querySelectorAll('.modal');
                var instances = M.Modal.init(elems, options);
            });

            // Or with jQuery

            $(document).ready(function(){
                $('.modal').modal();
            });

            $(".tabs>.indicator").css("background-color", '#4fc3f7');

            $("#form").validate({
                rules: {
                    login: {
                        required: true,
                        minlength: 6,
                        maxlength: 12
                    },
                    email: {
                        required: true,
                        email: true
                    },
                    password: {
                        required: true,
                        minlength: 6,
                        maxlength: 20
                    },
                    r_password: {
                        required: true,
                        equalTo: "#icon_password",
                        minlength: 6,
                        maxlength: 20
                    }
                },
                messages: {
                    login: {
                        required: "Поле 'Login' обязательно к заполнению",
                        minlength: "Введите не менее 6-ти символов в поле 'Login'",
                        maxlength: "Введите не больше 12-ти символов в поле 'Login'"
                    },
                    email: {
                        required: "Поле 'Email' обязательно к заполнению",
                        email: "Необходим формат адреса Email"
                    },
                    password: {
                        required: "Поле 'Password' обязательно к заполнению",
                        minlength: "Введите не менее 6-ти символов в поле 'Password'",
                        maxlength: "Введите не больше 20-ти символов в поле 'Password'"
                    },
                    r_password: {
                        required: "Поле 'Repeat password' обязательно к заполнению",
                        minlength: "Введите не менее 6-ти символов в поле 'Repeat password'",
                        maxlength: "Введите не больше 20-ти символов в поле 'Repeat password'",
                        equalTo: "Поле 'Repeat password' не совподают!"
                    }
                }

            });

            /*
            $("form").submit(function(){
                if($("#form").valid()){
                    $.ajax({
                        type: "POST",
                        url: "aith_registration.php",
                        data: $(this).serialize()
                    }).done(function(data) {
                        //console.log(data);
                        var response = JSON.parse(data);
                        console.log(response);
                        if(response.status === 'error') {
                            //console.log(response.error);
                            if (response.error == 'email exists') {
                                M.toast({html: 'Пользователь с таким email уже существует!'});
                                //$("#icon_email-error").text("Новое содержимое элемента label");
                            } else if (response.error == 'users exists'){
                                M.toast({html: 'Пользователь с таким login уже существует!'});
                            } else {
                                M.toast({html: 'Пользователь с таким email и login уже существует!'});
                            }
                        } else {
                            M.toast({html: 'Поздравляю, вы зарегистрировались!'});
                            //console.log('success');
                        }

                    });
                    return false;
                }
            });
            */

            $("form").submit(function(){
                if($("#form").valid()){
                    post_query('aulogin', 'register', 'login.email.password');
                }
            });
        });
    </script>

<?php bot(); ?>