<?php top('Авторизация');?>

<script src="/js/admin_login.js"></script>

<div class="row">

    <div class="col s12 m12 l6 xl5">
        <div class="container">
            <div class="row " id="top-height">
                <ul class="tabs ">
                    <li class="tab col s6 "><a class="active light-blue-text text-lighten-2" href="#test-swipe-1">Авторизация</a></li>
                </ul>
                <div id="test-swipe-1" class="col s12">
                    <form class="col s12" id="form">
                        <div class="row">
                            <div class="input-field row s6">
                                <i class="material-icons prefix">markunread</i>
                                <input id="email" name="email" type="email" class="validate">
                                <label id="email_error" for="email">Email</label>
                                <span class="helper-text"><label id="email-error" class="error" for="email"></label></span>
                            </div>
                            <div class="input-field row s6">
                                <i class="material-icons prefix">lock</i>
                                <input id="password" name="password" type="password" class="validate">
                                <label id="password_error" for="password">Password</label>
                                <span class="helper-text"><label id="password-error" class="error" for="password"></label></span>
                            </div>
                        </div>
                        <div>
                            <div class="col s6 left-align">
                                <p>
                                    <label>
                                        <input name="checkboxes" type="checkbox" />
                                        <span>Запомнить</span>
                                    </label>
                                </p>
                            </div>
                            <div class="col s6 right-align">
                                <button class="btn waves-effect waves-light btn-large" id="submit" type="submit" name="reg_submit">Регистрация
                                    <i class="material-icons right">send</i>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>

</div>

<script type="text/javascript" src="/js/jquery.validate.min.js"></script>
<script>
    $(document).ready(function(){
        $('.tabs').tabs({
    });

    $(".tabs>.indicator").css("background-color", '#4fc3f7');

    $("#form").validate({
        rules: {
            email: {
                required: true,
                email: true
            },
            password: {
                required: true,
                minlength: 6,
                maxlength: 20
            }
        },
        messages: {
            email: {
                required: "Поле 'Email' обязательно к заполнению",
                email: "Необходим формат адреса Email"
            },


            password: {
                required: "Поле 'Password' обязательно к заполнению",
                minlength: "Введите не менее 6-ти символов в поле 'Password'",
                maxlength: "Введите не больше 20-ти символов в поле 'Password'"
            }
        }
    });
        $("form").submit(function(){
            if($("#form").valid()){
                post_query('aulogin', 'login', 'email.password');
            }
        });
    });
</script>



<?php bot(); ?>