<?php top_photosession('ФотоСессия'); ?>

    <div class="col">
        <div class="container">
            <div class="row">
                <form id="form">
                    <div class="col s12 m12 l6 xl6">

                        <div class="input-field col s12">
                            <input name="name" id="name" type="text" class="validate">
                            <label for="name">Название</label>
                            <span class="helper-text"><label id="name-error" class="error" for="icon_name"></label></span>
                        </div>

                         <div class="col s12 m12 l12 xl12">
                            <div class="file-field input-field">
                                <div class="btn purple darken-1">
                                    <span>File</span>
                                    <input type="file" name="image" multiple>
                                </div>
                                <div class="file-path-wrapper ">
                                    <input name="file_image" class="file-path validate " type="text" placeholder="Upload one or more files">
                                </div>
                            </div>
                        </div>


                        <div class="center-align">
                            <button class="btn waves-effect waves-light btn-large pulse purple darken-1" type="submit" name="send">ОТПРАВИТЬ
                                <i class="material-icons right">send</i>
                            </button>
                        </div>
                    </div>

                </form>
            </div>

        </div>
    </div>


    <script>
    document.addEventListener('DOMContentLoaded', function() {
        var elems = document.querySelectorAll('.materialboxed');
        var instances = M.Materialbox.init(elems, options);
    });

    $(document).ready(function(){
        $('.materialboxed').materialbox();
    });
</script>
<?php bot_photosession(); ?>