<?php top_admin('R2 sevices');?>
<div class="col">
    <div class="container">
        <div class="row">
            <div class="col s12 m12 l6 xl6">
                <div class="input-field col s12">
                    <select multiple>
                        <option value="" disabled selected>Сервер</option>
                        <option value="1">Югенес</option>
                        <option value="2">Метеос</option>
                        <option value="3">Arena</option>
                        <option value="4">Arena STORM</option>
                        <option value="5">Bjorn</option>
                    </select>
                </div>
                <div class="input-field col s12">
                    <select multiple>
                        <option value="" disabled selected>Персонаж</option>
                        <option value="1">Рыцарь</option>
                        <option value="2">Рейнджер </option>
                        <option value="3">Маг </option>
                        <option value="4">Призыватель </option>
                        <option value="5">Ассасин </option>
                    </select>
                </div>
                <div class="input-field col s12">
                    <select multiple>
                        <option value="" disabled selected>Ф.И.О.</option>
                        <option value="1">Не установленно</option>
                        <option value="2">Установленно, данные знаю</option>
                        <option value="3">Установленно, данные не знаю</option>
                    </select>
                </div>
                <div class="input-field col s12">
                    <select multiple>
                        <option value="" disabled selected>Паспорт</option>
                        <option value="1">Не установленно</option>
                        <option value="2">Установленно, данные знаю</option>
                        <option value="3">Установленно, данные не знаю</option>
                    </select>
                </div>
                <div class="input-field col s12">
                    <select multiple>
                        <option value="" disabled selected>КОСЫ</option>
                        <option value="1">Нет</option>
                        <option value="2">Да (БИГВАР)</option>
                        <option value="3">Да (НЕЙТРАЛ)</option>
                        <option value="4">Да (ЛИЧНЫЙ)</option>
                    </select>
                </div>
                <div class="input-field col s12">
                    <select>
                        <option value="1"selected>Валюта</option>
                        <option value="2">Рубли</option>
                        <option value="3">Серебро</option>
                    </select>
                </div>
            </div>

            <div class="col s12 m12 l6 xl6">
                <h5>Уровень персонажа</h5>
                <div id="range_level"></div>

                <h5>Трофей: Маска</h5>
                <div id="range_trophy_mask"></div>

                <h5>Трофей: Золотая корона</h5>
                <div id="range_trophy_zk"></div>

                <h5>Цена</h5>
                <div class="col s12 m12 l12 xl12 range_select">
                    <div class="col s6 m6 l6 xl6 ">
                        <div class="input-field col s12">
                            <input id="price_from" name="price_from" type="text" class="validate">
                            <label for="price_from">от</label>
                        </div>
                    </div>
                    <div class="col s6 m6 l6 xl6 ">
                        <div class="input-field col s12">
                            <input id="price_to" name="price_to" type="text" class="validate">
                            <label for="price_to">до</label>
                        </div>
                    </div>
                </div>

                <div class="row center-align">
                    <button class="btn waves-effect waves-light pulse light-blue lighten-2" type="submit" name="action">Поиск
                        <i class="material-icons right">search</i>
                    </button>
                </div>
            </div>
        </div>
        <div class="row">
            <?php

            $id = $_GET['id'];
            if (isset($id)){
                $delete_element = "DELETE FROM `garant` WHERE garant_id = '$id'";
                $delete = mysqli_query($link, $delete_element);
            }
            $query_equipment = "SELECT * FROM `garant` WHERE visible = 1";
            $data_equipment = mysqli_query($link,$query_equipment);
            if (mysqli_num_rows($data_equipment) > 0) {
                $row_equipment = mysqli_fetch_array($data_equipment);
                do {
                    echo '
                <div class="col s12 m6 l4 xl4">
                    <div class="card hoverable">
                        <div class="card light-blue lighten-2">
                            <div class="card-content white-text">
                                <span class="card-title center-align">'.$row_equipment["buy_sell"].'</span>
                            </div>
                        </div>
                        <div class="card-image center-align">
                            <table>

                                <tbody>
                                <tr>
                                    <td>Сервер</td>
                                    <td>' . $row_equipment["server"] . '</td>
                                </tr>
                                <tr>
                                    <td>Класс</td>
                                    <td>' . $row_equipment["class"] . '</td>
                                </tr>
                                <tr>
                                    <td>Уровень</td>
                                    <td>' . $row_equipment["level"] . '</td>
                                </tr>
                                <tr>
                                    <td>Питомец</td>
                                    <td>' . $row_equipment["pet"] . '</td>
                                </tr>
                                <tr>
                                    <td>Ур. питомца</td>
                                    <td>' . $row_equipment["level_pet"] . ' </td>
                                </tr>
                                <tr>
                                    <td>Камень</td>
                                    <td>' . $row_equipment["stone_pet"] . '</td>
                                </tr>
                                <tr>
                                    <td>Грейд</td>
                                    <td>' . $row_equipment["pet_grade"] . '</td>
                                </tr>
                                <tr>
                                    <td>Маска</td>
                                    <td>' . $row_equipment["mask"] . '</td>
                                </tr>
                                <tr>
                                    <td>Золотая корона</td>
                                    <td>' . $row_equipment["gold_crown"] . '</td>
                                </tr>
                                <tr>
                                    <td>Ф.И.О.</td>
                                    <td>' . $row_equipment["fio"] . '</td>
                                </tr>
                                <tr>
                                    <td>Паспорт</td>
                                    <td>' . $row_equipment["pasport"] . '</td>
                                </tr>
                                <tr>
                                    <td>КОСы</td>
                                    <td>' . $row_equipment["kos"] . '</td>
                                </tr>
                                <tr>
                                    <td>Цена</td>
                                    <td>' . $row_equipment["price"].' '. $row_equipment["course"] . '</td>
                                </tr>
                                <tr>
                                    <td>Связь</td>
                                    <td><a href="' . $row_equipment["contact"] . '">' . $row_equipment["name_user"] . '</a></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="row center-align">
                            <a href="#'.$row_equipment["garant_id"].'" class="waves-effect waves-light btn-floating btn-large pulse red lighten-1 modal-trigger"><i class="material-icons">delete_sweep</i></a>
                        </div>
                        <!----------------- Delete --------------------->
                        <div id="'.$row_equipment["garant_id"].'" class="modal">
                            <div class="modal-content">
                                <h4>Удалить запись?</h4>
                                <p>Вы дейтсвительно хотите удалить данную запись?</p>
                            </div>
                            <div class="modal-footer">
                                <a class="modal-close waves-effect waves-green btn-flat">Disagree</a>
                                <a href="?id='.$row_equipment["garant_id"].'" class="modal-close waves-effect waves-green btn-flat">Agree</a>
                            </div>
                        </div>
                    </div>
                </div>
                       ';
                } while ($row_equipment = mysqli_fetch_array($data_equipment));
            }
            ?>
        </div>
    </div>
</div>


<script type="text/javascript" src="/js/nouislider.js"></script>
<script type="text/javascript" src="/js/range.js"></script>
<script type="text/javascript" src="/js/ion.rangeSlider.min.js"></script>
<script>
    $(document).ready(function () {
        $('.dropdown-trigger').dropdown();
        $('select').formSelect();

        $("#range_level").ionRangeSlider({
            type: "double",
            grid: true,
            min: 0,
            max: 200,
            from: 50,
            to: 150
        });


        $("#range_trophy_mask").ionRangeSlider({
            type: "double",
            grid: true,
            min: 0,
            max: 950,
            from: 238,
            to: 713
        });

        $("#range_trophy_zk").ionRangeSlider({
            type: "double",
            grid: true,
            min: 0,
            max: 950,
            from: 238,
            to: 713
        });

        document.addEventListener('DOMContentLoaded', function() {
            var elems = document.querySelectorAll('.modal');
            var instances = M.Modal.init(elems, options);
        });

        // Or with jQuery

        $(document).ready(function(){
            $('.modal').modal();
        });
    });
</script>

<?php bot(); ?>
